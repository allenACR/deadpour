define(['custom', 'threeJS', 'threeJS_stats', 'orbitControls', 'postprocessing', 'shaders', 'tweenJS'], function(custom, threeJS, threeJS_stats, orbitControls, gameModalCtrl, app) {
	var fileName  = 'game';
  	custom.logger(fileName + "Controller: Init");
	  return {
	  	///////////////////////////////////////
	    apply: function(app) {
			custom.logger(fileName + "Controller: Loaded");
				app.controller(fileName + 'Controller', function($scope, $modal, $compile, $injector, $position, cfpLoadingBar, stBlurredDialog, psResponsive, SmoothScroll, toaster, uiModalCtrl, gameModalCtrl, $timeout, $parse, $interval, $window, $http, $timeout) {

					// variables
					$scope.variables = {

						onload:function(){
							$scope.variables.system();
							$scope.variables.camera();
							$scope.variables.io();
							$scope.variables.canvas();
							$scope.variables.grid();
							$scope.variables.physics();
							
							// USE JQUERY TO ALWAYS GET RIGHT HEIGHT - SOMETIMES BINDING DOESN'T WORK
							$('#main-wrapper').css('height', $scope.viewport_height);
							$('#game-grid-container').css('height', $scope.viewport_height);

						},

						system:function(){
							$scope.screenSize = psResponsive;
							$scope.pathing_tile = 'media/game/tile/';
							$scope.pathing_particle = 	'media/game/particle/';
							$scope.pathing_textures = 	'media/game/textures/';
							$scope.pathing_skybox =  	'media/game/skybox/';
							$scope.pathing_threeJS_json = 'threeJS/json/';

							$scope.aspectWidth =  $( window ).width();
							$scope.aspectHeight = $scope.aspectWidth * .5625;
							$scope.aspectMaxWidth = parseInt($('#game-wrapper').css('max-width'));
							$scope.aspectMinWidth = parseInt($('#game-wrapper').css('min-width'));														
							
							$scope.layer_bg1  = 100;
							$scope.layer_main = 1000;
							$scope.layer_fg1  = 2000;
							$scope.layer_fg2  = 3000;

							$scope.loadReady = false;
							$scope.loadComplete = 0;
							$scope.assetsLoaded = 0;
							
							$scope.canvasInventory = [];
							$scope.hiddenWindows = [];
							
							 
							$scope.isBrowserActive = true;
							$scope.runInBackground = false;
							$scope.globalClock =  new THREE.Clock();			
						},

						grid: function(){
							$scope.tile_size = 50;
						},

						camera:function(){

							// height of display
							$scope.viewport_height = $(window).height();

							// container sizes
							$scope.container_width  = $('#game-wrapper').width();
							$scope.container_height = $scope.viewport_height;
							$scope.container_border_size = 5;

							// canvas size - as long as their in proportion it can be any size
							// currently its 1.8:1 width/height ratio
							$scope.mapSize_w_default = $scope.container_width;
							$scope.mapSize_h_default = $scope.viewport_height;

							// camera attributes
							$scope.camera_perspective = 200; 
							$scope.camera_isAnimated;

                            // touching window
                            $scope.touchingWindow;



						},

						io:function(){
							$scope.cameraType = "rotate";
							$scope.mouseCoords = {x: 0, y:0};
							$scope.touchingObject = null;
							$scope.togglePerspective = false;
							$scope.control_buffer = [];
							$scope.isControlActive = false;
							$scope.isUserInput = true;
							$scope.progress = {actual: .5};
							
						},

						canvas:function(){
							$scope.speed_x = .01;
							
							$scope.canvasData = [];
							$scope.canvasData['order'] = [];
							$scope.activeCanvas;
							$scope.currentLayout; 
							$scope.aspectFix = 0;
						},
						
						physics: function(){
							
							$scope.globalPhysics = {
								gravity: {
									x: 0,
									y: 1,
									z: 0
								}
							};	
							
							$scope.globalCamera = false;								
	
						}

					};

					// all data request go here
					$scope.init = function(){
						custom.startLoggerGroup('WebGL');
                        custom.startDraggables();
						$scope.numberOfCanvases = 1;
						$scope.loadCompleteNeeded = 2 + $scope.numberOfCanvases; 	// SHOULD MATCH NUMBER OF LOAD REQUESTS in INIT
						$scope.loadPercentage = 0;
						$scope.variables.onload();

						$scope.load.get_tileData( "setName", function(data){
							$scope.tileData = data;
							$scope.system.checkForComplete();
						});

						$scope.load.get_mapData( "mapName", function(data){
							$scope.mapData = data;
							$scope.system.checkForComplete();
						});
						

						$scope.canvas.buildCanvasInit( $scope.numberOfCanvases );
						var i = $scope.canvasInventory.length; while(i--){
							$scope.canvas.addToScene( $scope.pathing_threeJS_json + 'cubeAndSphere1.json', $scope.canvasInventory[i].canvasName, false, function(){
								$scope.system.checkForComplete();
							});									
						}

																	
					};

					// when everything is ready
					$scope.ready = {
						run:function(){
							// run only once
							if (!$scope.loadReady){
								custom.endLoggerGroup('WebGL');
								// setup canvas		
								$scope.io.watchScreen(); 	
								$scope.io.watchMouse();		
								$scope.io.watchBrowser();
																	
								// size screen
								$scope.io.executeControlBuffer();
								$scope.io.addToControlBuffer([ "changeResolution", "med" ] );
								 	
								// change layout
								$scope.io.addToControlBuffer([ "changeLayout", "fullscreen1" ] );
								$scope.io.addToControlBuffer([ "changeFocus", $scope.canvasInventory[0].canvasName  ] );		
																		
								$scope.io.addToControlBuffer([ "hideWindow", ["#game-grid-bottom-info-a", 'help', true] ]);
								$scope.io.addToControlBuffer([ "hideWindow", ["#game-grid-bottom-info-b", 'Buttons', false] ]);
								$scope.io.addToControlBuffer([ "hideWindow", ["#game-grid-bottom-info-c", 'T/R/Z', false] ]);	
								
								$scope.io.addToControlBuffer([ "changeOverlayColor", ["canvasContainer_0", 'white', 2.5, 0]	 ]);
								$scope.io.addToControlBuffer([ "changeOverlayColor", ["canvasContainer_1", 'white', 2.5, 0]	 ]);
								$scope.io.addToControlBuffer([ "changeOverlayColor", ["canvasContainer_2", 'white', 2.5, 0]	 ]);


								$scope.canvas.init(function(){
																			
													
											$scope.grid.createMapFromData(function(map){
												console.log("Map has been built.  Object returned." );		
												
												
																										
												$scope.loadReady = true;
												$scope.ready.fullyLoaded();
											});
										
								
								});
							}
						},
						
						fullyLoaded:function(){
							$('.hideForLoad').each(function(){	$(this).removeClass('hideForLoad');	});
							console.timeEnd('Site loaded in');
							console.groupEnd('Site Info');


                            $scope.web.eventBuilder_initial();
                            $scope.web.eventBuilder_linkToComponent();


						},
					};

					// web
					$scope.web = {

						callModal:function(name){

								$scope.openConfirm = function () {
									stBlurredDialog.open();
								    var modalInstance = $modal.open({
								      templateUrl: 'changeResolutionModal.html',
								      controller: gameModalCtrl.changeResolutionCtrl()
								    });

									modalInstance.result.then(
										// VIA CLOSE
										function (returnData)
										{
											$scope.results(returnData);
										},
										// VIA DISMISS
										function ()
										{
											stBlurredDialog.close();
								  			custom.logger('Modal dismissed.');
								    	});
								};

								$scope.results = function(data){
									$scope.io.addToControlBuffer([ "changeResolution", data ] );
									toaster.pop('success', "Resolution changed to: " + data, '' );
								};

								$scope.openConfirm();
						},

                        resetEventBuilder:function(){
                            $scope.eb_step1 = false;
                            $scope.eb_step2 = false;
                            $scope.eb_step3 = false;
                            $scope.eb_step4 = false;
                            $scope.eb_step5a = false;

                            $scope.eb_componentList_selected = null;
                            $scope.eb_eventsList_selected = null;
                            $scope.eb_createdEvent = null;
                        },

                        returnListOfComponents:function(canvasName){

                            var allComponents = $scope.canvasData[canvasName].threeJS.allComponents;
                            var list = [];
                            for ( var i = 0; i <=  $scope.canvasData[canvasName].sceneComponents.componentCount; i++){
                                var name = allComponents["_" + i].component.name;
                                var type = allComponents["_" + i].component.type;
                                var object = allComponents["_" + i]

                                list.push( {name: type + name, data: object})
                            }
                            return list;
                        },

                        eventBuilder_initial:function(){
                            $scope.web.resetEventBuilder();
                            $scope.eb_step1 = true;
                            $scope.eb_componentList = $scope.web.returnListOfComponents($scope.activeCanvas);
                        },

                        eventBuilder_selectEvent:function(){
                            $scope.eb_step2 = true;
                            $scope.eb_eventsList =  $scope.events.listOfEvents();
                        },

                        eventBuilder_selectBind:function(){
                            $scope.eb_step3 = true;
                            var component   = $scope.eb_componentList_selected,
                                eventObj    = $scope.eb_eventsList_selected;

                            $scope.eb_createdEvent =  eventObj
                        },

                        eventBuilder_eventBind:function(type){
                            $scope.eb_step4 = true;
                            var component   = $scope.eb_componentList_selected,
                                object = component.data.component,
                                eventObj =  $scope.eb_createdEvent;

                                // bind event
                                event = $scope.events[eventObj.functionName](object);
                                createdEvent =   { event: event };
                                object._events[type].addBehavior(createdEvent, type);

                            // clear and hide
                            toaster.pop('success', type + ' event added!' , 'Nicely done!' );
                            $scope.web.eventBuilder_initial();
                        },

                        eventBuilder_linkToComponent:function(){
                            $scope.eb_step2a = true;
                            $scope.eb_linkList = $scope.web.returnListOfComponents($scope.activeCanvas);
                        },

                        eventBuilder_selectLink:function(){

                        },

                        eventBuilder_linkBind:function(type){
                            var component   = $scope.eb_componentList_selected,
                                    object = component.data.component,
                                linkComponent = $scope.eb_linkList_selected,
                                    linkObject = linkComponent.data.component,
                                eventObj =  $scope.eb_createdEvent;

                                // bind event
                                event = $scope.events[eventObj.functionName](object, linkObject);
                                createdEvent =   { event: event };
                                object._events[type].addBehavior(createdEvent, type);

                                // clear and hide
                                toaster.pop('success', type + ' event added!' , 'Nicely done!' );
                                $scope.web.eventBuilder_initial();

                        }

                    };
					
					// system
					$scope.system = {
												
					    // add angularhooks post load
					    injectAngular:function(content, appendLocation) {
					    	
					        var element = $('body');
					        var scope = element.scope;
					      
					        var injector = element.injector();
					        var compile = injector.get('$compile');
					        	
					        	
					        	
					       		compile(content)($scope).appendTo(appendLocation);
					    },						
						
						returnRandomNumber:function(value, offset){
							var o = 0;
							if (offset){
								o = value/2;
							}
							n = Math.floor((Math.random() * value) + 1 - o);							
							return n;
						},
						
						trackMouse:function(evt) {
						    var pos = $scope.system.getMousePos(evt);	    
						    $scope.mouseCoords = {x: pos.x, y: pos.y};
						},
						
						getMousePos:function(evt) {	    
						    var doc = document.documentElement || document.body;
						    var pos = {
						        x: evt ? evt.pageX : window.event.clientX + doc.scrollLeft - doc.clientLeft,
						        y: evt ? evt.pageY : window.event.clientY + doc.scrollTop - doc.clientTop
						    };
						    return pos;
						},							
							
						getMouseVector: function(container){
							var container = $('#' + $scope.activeCanvas).parent(),
								offsetLeft = container.offset().left,
								offsetTop = container.offset().top,
								border = parseInt(container.css('border'));
								
																
								var x =   ( ($scope.mouseCoords.x - offsetLeft - border) / container.width() ) * 2 - 1,
								 	y = - ( ($scope.mouseCoords.y - offsetTop - border) / container.height() ) * 2 + 1;
								
								
							
							return {x: x, y: y};		
						},	
						
					
							
						getRandomColor:function() {
						    var letters = '0123456789ABCDEF'.split('');
						    var color = '#';
						    for (var i = 0; i < 6; i++ ) {
						        color += letters[Math.floor(Math.random() * 16)];
						    }
						    return color;
						},						
						
						hideWindow:function(name, description, allowRemove){							
							
							if ( !$(this).hasClass('hidden') ){
								$(name).animate({opacity: 0, transition: '0s'}, function(){
									$scope.hiddenWindows.push( {id: name, description: description, allowRemove: allowRemove, zIndex: $(this).css('z-index')} );
									$(this).css('z-index', -1);	
									$(this).addClass('hidden');
								});
							}
						},
						
						restoreWindow:function(name, index){
							$(name).removeClass('hidden');
							$(name).css('z-index', $scope.hiddenWindows[index].zIndex );
							$(name).animate({opacity: 1, transition: '0s'}, function(){						});	
							$scope.system.removeFromHiddenArray(index);
							
						},
						
						removeWindow:function(name, index){
							$scope.system.removeFromHiddenArray(index);
							//$(name).remove();
						},
						
						removeFromHiddenArray:function(index){
							$scope.hiddenWindows.splice(index, 1);
						},
						
						// loadComplete
						checkForComplete:function(){
							$scope.loadComplete ++;
							$scope.loadPercentage = Math.round( ($scope.loadComplete/$scope.loadCompleteNeeded)*100 );
							if ($scope.loadComplete >= $scope.loadCompleteNeeded){
								$scope.ready.run();
							};
						},

						// refresh all angular hooks
						refreshApp:function(){
							if(!$scope.$$phase) {
								$scope.$apply();
							};
						},
					};
					
					// tweens
					$scope.tweens = {
						
						construct: function(obj, instructions){
																
								var defaults;

								// get data 		
								defaults = obj.defaults,
								data = {px: defaults.position.x, py: defaults.position.y, pz: defaults.position.z, rx: defaults.rotation.x, ry: defaults.rotation.y, rz: defaults.rotation.z};
								
						
										
								// override existing tween data if it exists			
								var tweenData = obj.tweenData;
								if (tweenData.tween != null){
									tweenData.tween.stop();
								};
								
								// build components
								var components = [];
								var i = instructions.length; while(i--){
									components.push(instructions[i])
								}
								
								// build array
								var tweenArray = [];													
								var i = components.length; while (i--){

									tweenInfo =  $scope.tweens.format(obj, defaults, data, components[i]);
									tweenArray.push(tweenInfo);		
								} 			
	
								// create and execute
								$scope.tweens.create(tweenArray, data, obj);
																			
						},
						
											
						format:function(obj, defaults, data, details){

							var tween = {
									name: details.name,														
									duration: details.duration,
									delay: details.delay,
									tweenType: TWEEN.Easing[details.tweenType][details.tweenTail],
									startFrom: 	{px: data.px, 	py: data.py, 		pz: data.pz, 	rx: data.rx,	ry: data.ry,	rz: data.rz},
									endAt: 		{	px: data.px + details.coords.x, py: data.py + details.coords.y, pz: data.pz + details.coords.z,	
													rx: data.rx + details.rotate.x,	ry: data.ry + details.rotate.y,	rz: data.rz + details.rotate.z
												},					
									onUpdate: function(){ 
										obj.position.x = data.px; obj.position.y = data.py; obj.position.z = data.pz; 
										obj.rotation.set(data.rx * Math.PI / 180 , data.ry * Math.PI / 180, data.rz * Math.PI / 180);
									},
									onComplete: details.onComplete,								
									startNow: details.startNow,	
									chain: details.chainWith						
								};
								
								return tween;


						},
						
						listOfTweens:function(){
							
						},
						
						deleteTweens:function(){
							
						},
						
						create:function(tweenArray, position, object){
							//TWEEN.removeAll();

							// build tweens
							var tweenObject = {order: []};
							i = tweenArray.length; while (i--){
								var tweenInfo = tweenArray[i];
								var tweenLoop = function(){							  
								  return{
								    update:tweenInfo.onUpdate
								  };
								}();
								
								// 								
								tween = new TWEEN.Tween(position)
									.delay(tweenInfo.delay)
									.to(tweenInfo.endAt, tweenInfo.duration)
									.easing(tweenInfo.tweenType)
									.onUpdate( tweenLoop.update )
									.onComplete( tweenInfo.onComplete );								
								
								tweenObject[tweenInfo.name] = {
									tween: tween,
									chain: tweenInfo.chain,
									startNow: tweenInfo.startNow
								};
								
								tweenObject["order"].push(tweenInfo.name);
								
							}

							
							// add chains if not null
							i = tweenObject.order.length; while (i--){
								var name = tweenObject.order[i];
								var t = tweenObject[name];
								
								if (t.chain != null){
									t.tween.chain(tweenObject[t.chain].tween);
								}								
							}
							
							i = tweenObject.order.length; while (i--){
								var name = tweenObject.order[i];
								var t = tweenObject[name];
								
								if (t.startNow){
									t.tween.start( $scope.globalClock.getElapsedTime()*1000 );
								}	
								
														
							}
							
							// asssign tween position
							var allTweens = TWEEN.getAll();
							object.tweenData.tween = allTweens[allTweens.length - 1];



						},
						
					},
						
					// input/output
					$scope.io = {
						
						watchBrowser:function(){
							$(window).on("blur focus", function(e) {
							    var prevType = $(this).data("prevType");
							
							    if (prevType != e.type) {   //  reduce double fire issues
							        switch (e.type) {
							            case "blur":
							            	$scope.isBrowserActive = false;
							            	$scope.globalClock.stop();
							                $scope.canvas.pauseClocks();
							                break;
							            case "focus":
							            	$scope.globalClock.start();
							            	$scope.isBrowserActive = true;
							                $scope.canvas.resumeClocks();
							                break;
							        }
							    }
							
							    $(this).data("prevType", e.type);
							});
						},
						
						
						watchMouse:function(){
							document.onmousemove = $scope.system.trackMouse;
						},
						
						// monitor screen size
						watchScreen:function(){
							$scope.$watch(function(){
							       return $window.innerWidth;
							    }, function(value) {

									var i = $scope.canvasData.order.length; while (i--){

										// get canvas object
										var canvasObj = $scope.canvasData.order[i].object;
										
										// resize canvas			
										$scope.canvas.resizeCanvas(i, $scope.resolution_x, $scope.resolution_y);
									}	
							


							});
						},

						// select camera attribute
						selectCameraType:function(type){
							toaster.pop('success', type + ' selected.' , 'Use the arrow keys!' );
							$scope.cameraType = type;
						},
						
						
										
						// keypresses
						keypressCallback: function($event){
							
							
							if($scope.isUserInput){
								$event.preventDefault();
								var keyPress = $event.which,
										dir;
										if (keyPress == 37){dir = "l";}
										if (keyPress == 38){dir = "u";}
										if (keyPress == 39){dir = "r";}
										if (keyPress == 40){dir = "d";}

								//$scope.camera.keys(dir);
								$scope.canvas.keys(dir);
							}
						},

						// add to control buffer
						addToControlBuffer:function( actionToAdd, applyBufferLimit){
								if (applyBufferLimit != undefined || applyBufferLimit != null){
									if ($scope.control_buffer.length <= applyBufferLimit){
										$scope.control_buffer.push(actionToAdd);
									}
								}
								else{
									$scope.control_buffer.push(actionToAdd);
								}
						},

						// execute what's in t
						executeControlBuffer:function(){
							$interval(function(){
							if (!$scope.isControlActive && $scope.control_buffer.length > 0){
								 	$scope.isControlActive = true;
								 	cPackage 	= $scope.control_buffer[0];
								 	action 		= cPackage[0];
								 	data   		= cPackage[1];
								 	complete 	= cPackage[2];
								 	
									switch(action) {
										
										// system 
										case "hideWindow":
											 $scope.system.hideWindow(data[0], data[1], data[2]);
											 actionComplete();
										break;									
									    
									    case "delay":
											$timeout(function(){
												actionComplete();
											}, data);
									    break;
									    
									    case "userInput":{
											$scope.isUserInput = data;
											actionComplete();	
									    }									    
									    //
										
										
										// canvas
										case "changeOverlayColor":
											// data[0]: canvasName data[1]: color data[2]: transistion time data[3]: opacity																					
											$('#' + data[0]).parent().find('.gameMask').css({	'background-color': data[1], 'transition': data[2] + 's', 'opacity': data[3]	});											
											actionComplete();																																															
										break;
																				
									    case "rtzCanvas":
									    		$timeout(function(){									
													newCoords = data;	
													
													
													
													$scope.canvasData[$scope.activeCanvas].setting = {
														p:  newCoords.p,
														t:  newCoords.t,
														rX: newCoords.rX, rY: newCoords.rY, rZ: newCoords.rZ,
														tX: newCoords.tX, tY: newCoords.tY, tZ: newCoords.tZ,
														cX: newCoords.cX, cY: newCoords.cY, cZ: newCoords.cZ,
														offsetX: 0, offsetY: 0,										
													};													
													
													$scope.canvas.applyCSSToCanvas($scope.activeCanvas, function(){									
														actionComplete();	
													});		
												}, 1);
																			    
									    break;										
										
										
										// webGL
									    case "changeResolution":    	
									       	$scope.canvas.changeResolution(data, function(){
													actionComplete();
											});
									    break;

									    case "changeFocus":
									    	$scope.canvas.changeFocus(data);									    	
									    	actionComplete();	
									    break;
									   
									    case "changeLayout":									  
									   		$scope.canvas.changeLayout(data);									   			 									   			 
									   		actionComplete();	
									    break;
									    
									    case "clearCanvas":								  
									   		$scope.canvas.clearScene(data, function(){ 
									   			actionComplete();	
									   		});							   			 									   			 									   		
									    break;									    
									    
									    case "rebuildCanvas":				  	
									    	// IF not empty
									    	if ( $scope.canvasData[data[1]].status != 'default'){
									    		$scope.canvas.clearScene(data[1], function(){ 
													$scope.canvas.addToScene( $scope.pathing_threeJS_json + data[0], data[1], false, function(){
														actionComplete();	
														$scope.canvas.constructScene(data[1], function(){
															$scope.io.addToControlBuffer([ "changeFocus", data[1] ] );
															$scope.io.addToControlBuffer([ "changeLayout", $scope.currentLayout ] );		
														});
													});	
									    		});		
									    	}
									    	// if already cleared, then just rebuild
									    	else{
												$scope.canvas.addToScene( $scope.pathing_threeJS_json + data[0], data[1], false, function(){
													actionComplete();	
													$scope.canvas.constructScene(data[1], function(){
														$scope.io.addToControlBuffer([ "changeFocus", data[1] ] );
														$scope.io.addToControlBuffer([ "changeLayout", $scope.currentLayout ] );		
													});
												});										    		
									    	}			    												
										break;
										
									   	case "addToCanvas":								    				 
									   		$scope.canvas.addToScene( $scope.pathing_threeJS_json + data[0], data[1], true, function(){										   			
									   			actionComplete();										   			
									   		});							   			 									   			 									   		
									   	break;	
									   	
									   	case "togglePostProcessing":								    				 
											$scope.canvas.togglePostProcessing($scope.activeCanvas);
									    	actionComplete();					   			 									   			 									   		
									   	break;										   	
									    

									   	case "togglePause":									    				 
									   		$scope.canvas.togglePause(data);							   			
									   		actionComplete();										   									   			 									   			 									   		
									   	break;										    										

									   	case "pauseScene":
									   		$scope.canvas.pauseScene(data);
									   		actionComplete();	
									   	break;
									   	
									   	case "unpauseScene":
									   		$scope.canvas.unpauseScene(data);
									   		actionComplete();	
									   	break;									   	
									    
									   	case "pauseAllScenes":
									   		$scope.canvas.pauseAllScenes();
									   		actionComplete();	
									   	break;
									   
									   	case "unpauseAllScenes":
									   		$scope.canvas.unpauseAllScenes();
									   		actionComplete();
									   	break;
									   
									   
									  	case "toggleRender":								    				 
									   		$scope.canvas.toggleRender(data);					   			
									   		actionComplete();										   									   			 									   			 									   		
									  	break;
									  		
									  	case "activeRenderOnly":
									  		$scope.canvas.renderActiveOnly();										    				 						   			
									   		actionComplete();										   									   			 									   			 									   		
									  	break;										  		
									  		
									  	case "enableRender":									    				 							   			
									   		actionComplete();										   									   			 									   			 									   		
									  	break;
									  	
									  	case "disableRender":									    				 				   			
									   		actionComplete();										   									   			 									   			 									   		
									  	break;		
									  	
									  	case "enableAllRender":									    				 
									   		$scope.canvas.resumeAllRender();					   			
									   		actionComplete();										   									   			 									   			 									   		
									  	break;	
									  	
									  	case "disableAllRender":									  											    				
									   		$scope.canvas.stopAllRender();					   			
									   		actionComplete();										   									   			 									   			 									   		
									  	break;										  									  								  	
									  	
									  	
									  	// game specific
									  	case "changeGlobalGravity":									  		
									  		$scope.globalPhysics.gravity.y = data;
									  		actionComplete();
									  	break;	
									  	
									  	case "changeWind_X":									  		
									  		$scope.globalPhysics.gravity.x = data;
									  		actionComplete();
									  	break;		
									  	
									  	case "changeWind_Z":
											$scope.globalPhysics.gravity.z = data;
									  		actionComplete();									  	
									  	break;	
									  	
									  	
									  	case "changeFilter":
									  		var name = data[0],
									  			cData = $scope.canvasData[name],
												scene = cData.scene.scene,
												camera = cData.threeJS.camera,
												renderer = cData.threeJS.renderer,
												filters = cData.filters;
												
											var filterData = data[1],
												type = filterData.type,
												enabled = filterData.enabled,
												filterParameters = filterData.parameters;
													
												if (enabled == "toggle"){	
													if(filters[type].enabled){
														filters[type].enabled = false;
													}
													else{
														filters[type].enabled = true;
													}
												}
												else{
													filters[type].enabled = enabled;
													
												}
												
																								
												// change parameters
												if (filterParameters != null){	
													switch(type) {
														case "dotScreen":
															filters[type].parameters.scale = filterParameters.scale;
														break;	
														case "glitch":
															filters[type].parameters.goWild = filterParameters.goWild;															
														break;		
														case "RGBShift":
															//filters[type].parameters.parameter = filterParameters.parameter;															
														break;		
														case "sepia":
															//filters[type].parameters.parameter = filterParameters.parameter;
														break;		
														case "bloom":
															filters[type].parameters.strength = filterParameters.strength;
															filters[type].parameters.kernalSize = filterParameters.kernelSize;
															filters[type].parameters.sigma = filterParameters.sigma;
															filters[type].parameters.resolution = filterParameters.resolution;
														break;				
														case "bleach":
															filters[type].parameters.opacity = filterParameters.opacity;
														break;		
														case "brightness":
															filters[type].parameters.contrast = filterParameters.contrast;
														break;
														case "luminosity":
															//filters[type].parameters.parameter = filterParameters.parameter;
														break;
														case "colorCorrection":
															//filters[type].parameters.parameter = filterParameters.parameter;
														break;	
														case "colorify":														
															filters[type].parameters.color = filterParameters.color;
														break;	
														case "hueSaturation":
															filters[type].parameters.hue = filterParameters.hue;
															filters[type].parameters.saturation = filterParameters.saturation;
														break;	
														case "kaleido":
															//filters[type].parameters.parameter = filterParameters.parameter;
														break;		
														case "vignette":
															filters[type].parameters.darkness = filterParameters.darkness;
														break;	
														case "verticalBlur":
															filters[type].parameters.darkness = filterParameters.darkness;
														break;	
														case "horizontalBlur":
															filters[type].parameters.darkness = filterParameters.darkness;
														break;	
														case "focus":
															filters[type].parameters.darkness = filterParameters.darkness;
														break;																																																																																		
														
																																			
													}
												}
												
												
																							
												
												
												// update
												composer = $scope.canvas.updateFilters(scene, camera, renderer, filters);																			
												$scope.canvasData[name].composer = composer;
												
												// remove from q
												actionComplete();			
											
									  	break;							  										  									   
								
									}
								}
							},0);


							function actionComplete(){
								$scope.isControlActive = false;
								$scope.control_buffer.shift();
							};
						},

					};

					// canvas camera
					$scope.camera = {


						// TOGGLE PERSPECTIVE
						toggleTopdown: function(){
								$scope.togglePerspective = $scope.togglePerspective === false ? true: false;
								if($scope.togglePerspective){
									$scope.camera_perspective = 2000;
								}
								else{
									$scope.camera_perspective = 600;
								}
						},


					};

					// game grid
					$scope.grid = {

							// create map data
							createMapFromData:function(callback){

								mapData = $scope.mapData;
								tileData = $scope.tileData;

								var mapLayout = [];
								
								var i = mapData.length; while (i--){
										var n = mapData[i].length; while (n--){										

											 var tileData = 	$scope.tileData[mapData[i][n][0]];
											 var row = i;
											 var column = n;

											 mapLayout.push({texture: $scope.texture_pathing + tileData[0].fileName, location: [i, n], type: tileData[1].type});

										}
								}
								callback(mapLayout);

							},
					};

					// loading function
					$scope.load = {
						
						get_json: function(address, callback){

						  $http({method: 'GET', url: address}).
							    success(function(data, status, headers, config) {
							    	callback(true, data);
							    }).
							    error(function(data, status, headers, config) {
							    	callback(false, status);
						  });			
						  

							
						},
								
						get_mapData: function( loadParamters, callback ){

								// test map data
								var data = [
									[	[0, 1], [1, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1],[0, 1], [0, 1],[0, 1], [0, 1], [0, 1],[0, 1], [0, 1],[0, 1]  ],
									[	[0, 1], [1, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1],[0, 1], [0, 1],[0, 1], [0, 1], [0, 1],[0, 1], [0, 1],[0, 1]  ],
									[	[0, 1], [1, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1],[0, 1], [0, 1],[0, 1], [0, 1], [0, 1],[0, 1], [0, 1],[0, 1]  ],
									[	[0, 1], [1, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1],[0, 1], [0, 1],[0, 1], [0, 1], [0, 1],[0, 1], [0, 1],[0, 1]  ],
									[	[0, 1], [1, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1],[0, 1], [0, 1],[0, 1], [0, 1], [0, 1],[0, 1], [0, 1],[0, 1]  ],
									[	[0, 1], [1, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1],[0, 1], [0, 1],[0, 1], [0, 1], [0, 1],[0, 1], [0, 1],[0, 1]  ],
									[	[0, 1], [1, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1],[0, 1], [0, 1],[0, 1], [0, 1], [0, 1],[0, 1], [0, 1],[0, 1]  ],
									[	[0, 1], [1, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1],[0, 1], [0, 1],[0, 1], [0, 1], [0, 1],[0, 1], [0, 1],[0, 1]  ],
									[	[0, 1], [1, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1],[0, 1], [0, 1],[0, 1], [0, 1], [0, 1],[0, 1], [0, 1],[0, 1]  ],
									[	[0, 1], [1, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1],[0, 1], [0, 1],[0, 1], [0, 1], [0, 1],[0, 1], [0, 1],[0, 1]  ],
									[	[0, 1], [1, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1],[0, 1], [0, 1],[0, 1], [0, 1], [0, 1],[0, 1], [0, 1],[0, 1]  ],
									[	[0, 1], [1, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1],[0, 1], [0, 1],[0, 1], [0, 1], [0, 1],[0, 1], [0, 1],[0, 1]  ],
									[	[0, 1], [1, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1],[0, 1], [0, 1],[0, 1], [0, 1], [0, 1],[0, 1], [0, 1],[0, 1]  ],
									[	[0, 1], [1, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1],[0, 1], [0, 1],[0, 1], [0, 1], [0, 1],[0, 1], [0, 1],[0, 1]  ],
									[	[0, 1], [1, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1],[0, 1], [0, 1],[0, 1], [0, 1], [0, 1],[0, 1], [0, 1],[0, 1]  ],
									[	[0, 1], [1, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1],[0, 1], [0, 1],[0, 1], [0, 1], [0, 1],[0, 1], [0, 1],[0, 1]  ],
									[	[0, 1], [1, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1],[0, 1], [0, 1],[0, 1], [0, 1], [0, 1],[0, 1], [0, 1],[0, 1]  ],
									[	[0, 1], [1, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1],[0, 1], [0, 1],[0, 1], [0, 1], [0, 1],[0, 1], [0, 1],[0, 1]  ],
									[	[0, 1], [1, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1],[0, 1], [0, 1],[0, 1], [0, 1], [0, 1],[0, 1], [0, 1],[0, 1]  ],
									[	[0, 1], [1, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1],[0, 1], [0, 1],[0, 1], [0, 1], [0, 1],[0, 1], [0, 1],[0, 1]  ],
								];
								setTimeout(function(){
									callback(data);
								}, 0);
						},

						get_tileData: function( loadParameters, callback ){
							// TEXTURE DATA
							var data = [
									[ {fileName: 'grass-128x128.png'}, {type: 'grass', info: 'Basic grass.  No defense or offensive bonus.'} ],
									[ {fileName: 'grass-128x128.png'}, {type: 'blank', info: 'An empty void in time and space.'} ],
									[ {fileName: 'grass-128x128.png'}, {type: 'grass', info: '---'} ],
							];

							callback( data );
						},

					};



                    $scope.events = {

                        // return list of events
                        listOfEvents: function(){
                            list = [
                                {name: "Console Log",        functionName: 'consoleLog',    description: "Console logs the current object." },
                                {name: "Rotate on X-Axis",   functionName: 'rotateX',       description: "Rotate object on X-Axis." },
                                {name: "Copy rotation",      functionName: 'copyRotation',  description: "Mimic the exact rotation of another object." }
                            ]

                            return list;

                        },

                        // return a simple consoleLog
                        consoleLog:function(object){
                            var _return = function(){
                                console.log(object)
                            }
                            return _return;
                        },

                        //  copy movement
                        copyRotation:function(object, objectToCopy){
                            var _return = function(){
                                object.rotation.set(objectToCopy.rotation.x, objectToCopy.rotation.y, objectToCopy.rotation.z);
                            }
                            return _return;
                        },

                        // tween x
                        rotateX:function(object){
                            var _return = function(){
                                //Linear, Quadratic, Cubic, Quartic, Quintic, Sinusoidal, Exponential, Circular, Elastic
                                var instructions = [
                                    {	name: "step1", chainWith: "step2",
                                        startNow: true, duration: 2000, delay: 0,
                                        tweenType: "Exponential", tweenTail: "InOut",
                                        coords: {x: 0, y: 0, z: 0},
                                        rotate: {x: 180, y: 0, z:0 },
                                        onComplete: function(){ }
                                    },
                                    {	name: "step2", chainWith: "step3",
                                        startNow: false, duration: 2000, delay: 0,
                                        tweenType: "Quartic", tweenTail: "InOut",
                                        coords: {x: 0, y: 0, z: 0},
                                        rotate: {x: 180, y: 180, z:0 },
                                        onComplete: function(){ }
                                    },
                                    {	name: "step3", chainWith: "step4",
                                        startNow: false, duration: 2000, delay: 0,
                                        tweenType: "Elastic", tweenTail: "InOut",
                                        coords: {x: 0, y: 0, z: 0},
                                        rotate: {x: 180, y: 180, z: 180 },
                                        onComplete: function(){ }
                                    },
                                    {	name: "step4", chainWith: "step1",
                                        startNow: false, duration: 2000, delay: 0,
                                        tweenType: "Quartic", tweenTail: "InOut",
                                        coords: {x: 0, y: 0, z: 0},
                                        rotate: {x: 0, y: 0, z: 0 },
                                        onComplete: function(){ }
                                    },
                                ];

                                $scope.tweens.construct(object, instructions);


                            }
                            return _return;
                        },

                        // DEBUG ONLY - DO NOT PUT IN LIST OF EVENTS
                        //
                        selectOnClick:function(object){
                            var _return = function(){
                                $scope.currentlySelectedObject = object
                            }
                            return _return;
                        }

                    }

					// used for testing
					$scope.testing = {
						
						tween: function(){
							tweenArray = [];
						
							threeJS = $scope.canvasData[$scope.activeCanvas].threeJS,
							camera = threeJS.camera,
							light = threeJS.allUniques.mainLight.component,
							target = threeJS.allUniques.mainLight.target;
							
							

							//Linear, Quadratic, Cubic, Quartic, Quintic, Sinusoidal, Exponential, Circular, Elastic
							var instructions = [
								{	name: "step1", chainWith: "step2", 
									startNow: true, duration: 1000, delay: 0, 
									tweenType: "Exponential", tweenTail: "InOut", 
									coords: {x: -500, y: 0, z: 0}, 
									rotate: {x: 0, y: 0, z: 0},	
									onComplete: function(){ }	
								},
								{	name: "step2", chainWith: "step1", 
									startNow: false, duration: 1000, delay: 0, 
									tweenType: "Exponential", tweenTail: "InOut", 
									coords: {x: 500, y: 0, z: 0}, 
									rotate: {x: 0, y: 0, z: 0},	
									onComplete: function(){ }	
								}															
											
							];
							$scope.tweens.construct(light, instructions);
					
							var instructions = [
								{	name: "step1", chainWith: "step2",
									startNow: true, duration: 10000, delay: 0,
									tweenType: "Sinusoidal", tweenTail: "InOut",
									coords: {x: 300, y: 0, z: 300},
									rotate: {x: 0, y: 0, z: 0},
									onComplete: function(){ }
								},
								{	name: "step2", chainWith: "step1",
									startNow: false, duration: 10000, delay: 0,
									tweenType: "Sinusoidal", tweenTail: "InOut",
									coords: {x: -300, y: 0, z: -300},
									rotate: {x: 0, y: 0, z: 0},
									onComplete: function(){ }
								}

							];
							$scope.tweens.construct(camera, instructions);
										
							

						
		
						},
						
						changeFilter: function(type){
							
							if (type == "film"){
								$scope.io.addToControlBuffer([ "changeFilter", [$scope.activeCanvas, {type: type, enabled: 'toggle', parameters: null} ] 	] );
							}
							
							if (type == "dotScreen"){
								$scope.io.addToControlBuffer([ "changeFilter", [$scope.activeCanvas, {type: type, enabled: 'toggle', parameters: {scale: 2.5}} ] 	] );
							}	
							
							if (type == "glitch"){
								$scope.io.addToControlBuffer([ "changeFilter", [$scope.activeCanvas, {type: type, enabled: 'toggle', parameters: {goWild: true}} ] 	] );
							}	
							
							if (type == "RGBShift"){
								$scope.io.addToControlBuffer([ "changeFilter", [$scope.activeCanvas, {type: type, enabled: 'toggle', parameters: null} ] 	] );
							}	
							
							if (type == "sepia"){
								$scope.io.addToControlBuffer([ "changeFilter", [$scope.activeCanvas, {type: type, enabled: 'toggle', parameters: null} ] 	] );
							}	
							
							if (type == "bloom"){
								$scope.io.addToControlBuffer([ "changeFilter", [$scope.activeCanvas, {type: type, enabled: 'toggle', parameters: {strength: 2, kernelSize: 25, sigma: 40.0, resolution: 256}} ] 	] );
							}	
							
							if (type == "bleach"){
								$scope.io.addToControlBuffer([ "changeFilter", [$scope.activeCanvas, {type: type, enabled: 'toggle', parameters: {opacity: 2.0}} ] 	] );
							}								
							
							if (type == "brightness"){
								$scope.io.addToControlBuffer([ "changeFilter", [$scope.activeCanvas, {type: type, enabled: 'toggle', parameters: null} ] 	] );
							}	
							
							if (type == "luminosity"){
								$scope.io.addToControlBuffer([ "changeFilter", [$scope.activeCanvas, {type: type, enabled: 'toggle', parameters: null} ] 	] );
							}	
							
							if (type == "colorCorrection"){
								$scope.io.addToControlBuffer([ "changeFilter", [$scope.activeCanvas, {type: type, enabled: 'toggle', parameters: null} ] 	] );
							}	
							
							if (type == "colorify"){
								$scope.io.addToControlBuffer([ "changeFilter", [$scope.activeCanvas, {type: type, enabled: 'toggle', parameters: {color: 0xff00ff}} ] 	] );
							}								
																			
							if (type == "hueSaturation"){
								$scope.io.addToControlBuffer([ "changeFilter", [$scope.activeCanvas, {type: type, enabled: 'toggle', parameters: {hue: 0.5, saturation: 0.5}} ] 	] );
							}	
							
							if (type == "kaleido"){
								$scope.io.addToControlBuffer([ "changeFilter", [$scope.activeCanvas, {type: type, enabled: 'toggle', parameters: null} ] 	] );
							}	
							
							if (type == "mirror"){
								$scope.io.addToControlBuffer([ "changeFilter", [$scope.activeCanvas, {type: type, enabled: 'toggle', parameters: null} ] 	] );
							}	
							
							if (type == "vignette"){
								$scope.io.addToControlBuffer([ "changeFilter", [$scope.activeCanvas, {type: type, enabled: 'toggle', parameters: {darkness: 2.0}} ] 	] );
							}
							
							if (type == "horizontalBlur"){
								$scope.io.addToControlBuffer([ "changeFilter", [$scope.activeCanvas, {type: type, enabled: 'toggle', parameters: null} ] 	] );
							}	
							
							if (type == "verticalBlur"){
								$scope.io.addToControlBuffer([ "changeFilter", [$scope.activeCanvas, {type: type, enabled: 'toggle', parameters: null} ] 	] );
							}	

							if (type == "focus"){
								$scope.io.addToControlBuffer([ "changeFilter", [$scope.activeCanvas, {type: type, enabled: 'toggle', parameters: null} ] 	] );
							}																																																																													

							
						},
						
						togglePP: function(){
							$scope.io.addToControlBuffer([ "togglePostProcessing", $scope.activeCanvas ] );		
							
						},
						
						pause: function(sceneId){							
							$scope.io.addToControlBuffer([ "togglePause", $scope.activeCanvas ] );									
						},	
						pauseAll: function(sceneId){							
							//$scope.io.addToControlBuffer([ "pauseScene", $scope.activeCanvas ] );  //single		
							$scope.io.addToControlBuffer([ "pauseAllScenes" ] );					// all
						},	
						unpauseAll: function(sceneId){																				
							//$scope.io.addToControlBuffer([ "unpauseScene", $scope.activeCanvas ] );	// single
							$scope.io.addToControlBuffer([ "unpauseAllScenes" ] );	// all															
						},	
						
						render:function(){
							$scope.io.addToControlBuffer([ "toggleRender", $scope.activeCanvas ] );		
						},	
						renderOnly:function(){
							$scope.io.addToControlBuffer([ "activeRenderOnly" ] );		
						},																		
						renderAll:function(){
							$scope.io.addToControlBuffer([ "enableAllRender" ] );	
						},												
						unrenderAll:function(){
							$scope.io.addToControlBuffer([ "disableAllRender" ] );				
						},							
																								
						// gravity and wind
						changeGravity:function(type){	
								switch(type) {
								    case 0:
								    	$scope.io.addToControlBuffer([ "changeGlobalGravity", 1 ] );	
								    break;
								    case 1:
								    	$scope.io.addToControlBuffer([ "changeGlobalGravity", -1 ] );
								    break;
 									case 2:
 										$scope.io.addToControlBuffer([ "changeGlobalGravity", -0.00001 ] );
								    break;
								};
						},
						changeWindX:function(type){
							
							
								switch(type) {
								    case 0:
								    	$scope.io.addToControlBuffer([ "changeWind_X", -0.1 ] );
								    break;
								    case 1:
								        $scope.io.addToControlBuffer([ "changeWind_X",  0.1 ] );
								    break;
 									case 2:
								        $scope.io.addToControlBuffer([ "changeWind_X", -0 ] );
								    break;
								};
						},	
						changeWindZ:function(type){
								switch(type) {
								    case 0:
								    	$scope.io.addToControlBuffer([ "changeWind_Z", -0.1 ] );
								    break;
								    case 1:
								        $scope.io.addToControlBuffer([ "changeWind_Z",  0.1 ] );
								    break;
 									case 2:
								        $scope.io.addToControlBuffer([ "changeWind_Z", 0 ] );
								    break;
								};
						},												
						
						clear: function(sceneId){
								$scope.io.addToControlBuffer([ "clearCanvas", $scope.activeCanvas ] );
						},						
						rebuild: function(sceneId){
								$scope.io.addToControlBuffer([ "rebuildCanvas", ['basic.json', $scope.activeCanvas] ] );											
						},														
						addToCanvas:function(type){											
								switch(type) {
								    case 0:
										$scope.io.addToControlBuffer([ "addToCanvas", ['parts/plane.json', $scope.activeCanvas] ] );
								    break;									
								    case 1:
										$scope.io.addToControlBuffer([ "addToCanvas", ['parts/sphere.json', $scope.activeCanvas] ] );
								    break;
								    case 2:
								        $scope.io.addToControlBuffer([ "addToCanvas", ['parts/cube.json', $scope.activeCanvas] ] );
								    break;
								    
								    
								    case 10:
										$scope.io.addToControlBuffer([ "addToCanvas", ['parts/skybox.json', $scope.activeCanvas] ] );
								    break;								    
								    
								}																							
						},			
						addParticle:function(type){
								
								$scope.io.addToControlBuffer([ "addToCanvas", [type + '.json', $scope.activeCanvas] ] );	
													
						},
		
						// fullscreen
						screenLayout:function(type){

							if (type == 1){					
								$scope.io.addToControlBuffer([ "changeLayout", "splitWidth" ], 1);			
													
							};
							if (type == 2){	
								$scope.io.addToControlBuffer([ "enableAllRender" ] );			
								$scope.io.addToControlBuffer([ "changeLayout", "arranged" ], 1);								
												
							};		
							
							if (type == 3){																
								$scope.io.addToControlBuffer([ "enableAllRender" ] );	
								$scope.io.addToControlBuffer([ "changeLayout", "fullscreen1" ], 1);
								$scope.io.addToControlBuffer([ "changeFocus", $scope.canvasInventory[0].canvasName  ] );		
								$scope.io.addToControlBuffer([ "activeRenderOnly" ] );																															
							};	
							
							if (type == 4){												
								$scope.io.addToControlBuffer([ "enableAllRender" ] );		
								$scope.io.addToControlBuffer([ "changeLayout", "fullscreen2" ], 1);
								$scope.io.addToControlBuffer([ "changeFocus", $scope.canvasInventory[1].canvasName  ] );
								$scope.io.addToControlBuffer([ "activeRenderOnly" ] );																							
							};		
							
							if (type == 5){											
								$scope.io.addToControlBuffer([ "enableAllRender" ] );	
								$scope.io.addToControlBuffer([ "changeLayout", "fullscreen3" ], 1);
								$scope.io.addToControlBuffer([ "changeFocus", $scope.canvasInventory[2].canvasName  ] );
								$scope.io.addToControlBuffer([ "activeRenderOnly" ] );	
							};	
							
							if (type == 6){		
								$scope.io.addToControlBuffer([ "enableAllRender" ] );		
								$scope.io.addToControlBuffer([ "changeLayout", "mainBottom" ], 1);							
									
							};	

							if (type == 7){		
								$scope.io.addToControlBuffer([ "enableAllRender" ] );	
								$scope.io.addToControlBuffer([ "changeLayout", "mainTop" ], 1);						
															
							};																													

						},
						

					};

					// threejs canvas stuff
					$scope.canvas = {

						init:function(callback){
								
												
								console.time('Build Scenes') ;
								$scope.canvas.animateLoop();
								
								$scope.canvas.constructAllScenes(function(){										
									console.timeEnd('Build Scenes') ;
									
									callback();
								});


						},											
												
						minimizeAllLayout:function(){	
								var i =  $scope.canvasInventory.length; while(i--){
									data = {
										canvas: $('#' + $scope.canvasInventory[i].parentName),
										height: '0px',	width: '0px', 
										left: '0px', 	right: 'auto',
										bottom: '0px', top: 'auto',
										speed: 0, layer: 0									
									};
									$scope.canvas.animateCanvas(data);	
								}		
						},
						
						changeLayout:function(type){
							$scope.currentLayout = type;							
							switch(type) {
							    case "splitWidth":
								    $scope.canvas.minimizeAllLayout();
								    
								    if (  $scope.canvasInventory[0] != undefined){
										data = {
											canvas: $('#' + $scope.canvasInventory[0].parentName ),
											height: '75%',	width: '75%', 
											left: '25px', 	right: 'auto',
											bottom: '50px', top: 'auto',
											speed: 0, layer: 1									
										};
										$scope.canvas.animateCanvas(data);
									}
									
									if (  $scope.canvasInventory[1] != undefined){
										data = {
											canvas: $('#' + $scope.canvasInventory[1].parentName ),
											height: '75%',	width: '75%', 
											left: 'auto', 	right: '25px',
											bottom: '50px', top: 'auto',
											speed: 0, layer: 1										
										};
										$scope.canvas.animateCanvas(data);
									}	
								
									if (  $scope.canvasInventory[2] != undefined){
										data = {
											canvas: $('#' + $scope.canvasInventory[2].parentName ),
											height: '0%',	width: '0%', 
											left: 'auto', 	right: 'auto',
											bottom: 'auto', top: 'auto',
											speed: 0, layer: 0										
										};
										$scope.canvas.animateCanvas(data);	
									}	
							    break;
							    case "arranged":
								    $scope.canvas.minimizeAllLayout();
								    
								    if (  $scope.canvasInventory[0] != undefined){
										data = {
											canvas: $('#' + $scope.canvasInventory[0].parentName ),
											height: '100%',	width: '100%', 
											left: 'auto', 	right: 'auto',
											bottom: 'auto', top: '0px',
											speed: 0, layer: 2									
										};
										$scope.canvas.animateCanvas(data);
									}
									
									if (  $scope.canvasInventory[1] != undefined){
										data = {
											canvas: $('#' + $scope.canvasInventory[1].parentName ),
											height: '100%',	width: '100%', 
											left: 'auto', 	right: 'auto',
											bottom: 'auto', top: '0px',
											speed: 0, layer: 1										
										};
										$scope.canvas.animateCanvas(data);
									}	
								
									if (  $scope.canvasInventory[2] != undefined){
										data = {
											canvas: $('#' + $scope.canvasInventory[2].parentName ),
											height: '0%',	width: '0%', 
											left: 'auto', 	right: 'auto',
											bottom: 'auto', top: 'auto',
											speed: 0, layer: 0										
										};
										$scope.canvas.animateCanvas(data);	
									}
							    break;
							    case "fullscreen1":							    	
							    	$scope.canvas.minimizeAllLayout();
							    	if (  $scope.canvasInventory[0] != undefined){
										data = {
											canvas: $('#' + $scope.canvasInventory[0].parentName ),
											height: '100%',	width: '100%', 
											left: '0px', 	right: 'auto',
											bottom: 'auto', top: '0px',
											speed: 0, layer: 1									
										};
										$scope.canvas.animateCanvas(data);		
									}
							    break;	
							    
							    case "fullscreen2":
							    	$scope.canvas.minimizeAllLayout();
							    	if (  $scope.canvasInventory[1] != undefined){
										data = {
											canvas: $('#' + $scope.canvasInventory[1].parentName ),
											height: '100%',	width: '100%', 
											left: '0px', 	right: 'auto',
											bottom: 'auto', top: '0px',
											speed: 0, layer: 1									
										};
										$scope.canvas.animateCanvas(data);		
									}
							    break;
							    case "fullscreen3":
							    	$scope.canvas.minimizeAllLayout();
							    	if (  $scope.canvasInventory[2] != undefined){
										data = {
											canvas: $('#' + $scope.canvasInventory[2].parentName ),
											height: '100%',	width: '100%', 
											left: '0px', 	right: 'auto',
											bottom: 'auto', top: '0px',
											speed: 0, layer: 1									
										};
										$scope.canvas.animateCanvas(data);		
									}
							    break;		
							    					    
							    case "mainBottom":							    	
								    $scope.canvas.minimizeAllLayout();
								    
								    if (  $scope.canvasInventory[0] != undefined){
										data = {
											canvas: $('#' + $scope.canvasInventory[0].parentName ),
											height: '20%',	width: '20%', 
											left: '25px', 	right: 'auto',
											bottom: '50px', top: 'auto',
											speed: 0, layer: 1									
										};
										$scope.canvas.animateCanvas(data);
									}
									
									if (  $scope.canvasInventory[1] != undefined){
										data = {
											canvas: $('#' + $scope.canvasInventory[1].parentName ),
											height: '20%',	width: '20%', 
											left: 'auto', 	right: '25px',
											bottom: '50px', top: 'auto',
											speed: 0, layer: 1										
										};
										$scope.canvas.animateCanvas(data);
									}	
								
									if (  $scope.canvasInventory[2] != undefined){
										data = {
											canvas: $('#' + $scope.canvasInventory[2].parentName ),
											height: '100%',	width: '100%', 
											left: 'auto', 	right: 'auto',
											bottom: 'auto', top: 'auto',
											speed: 0, layer: 0										
										};
										$scope.canvas.animateCanvas(data);	
									}
									
							    break;		
							    
							    case "mainTop":
								    $scope.canvas.minimizeAllLayout();
								    
								    if (  $scope.canvasInventory[0] != undefined){
										data = {
											canvas: $('#' + $scope.canvasInventory[0].parentName ),
											height: '20%',	width: '20%', 
											left: '25px', 	right: 'auto',
											bottom: 'auto', top: '50px',
											speed: 0, layer: 1									
										};
										$scope.canvas.animateCanvas(data);
									}
									
									if (  $scope.canvasInventory[1] != undefined){
										data = {
											canvas: $('#' + $scope.canvasInventory[1].parentName ),
											height: '20%',	width: '20%', 
											left: 'auto', 	right: '25px',
											bottom: 'auto', top: '50px',
											speed: 0, layer: 1										
										};
										$scope.canvas.animateCanvas(data);
									}	
								
									if (  $scope.canvasInventory[2] != undefined){
										data = {
											canvas: $('#' + $scope.canvasInventory[2].parentName ),
											height: '100%',	width: '100%', 
											left: 'auto', 	right: 'auto',
											bottom: 'auto', top: 'auto',
											speed: 0, layer: 0										
										};
										$scope.canvas.animateCanvas(data);	
									}
							    break;								    						    								    						    

							}							
							

							
						},
						
						animateCanvas:function(json){
	
								var animateString = "",
									canvas 		= $(json.canvas),
									name = canvas.find('canvas').attr('id'),
									indexNum = 	canvas.find('canvas').attr('index');
									
									
																										
										canvas.css('transition', '0s');
										canvas.animate({
												'height': json.height, 'width': json.width, 
												'top': json.top, 'bottom':json.bottom,
												'left': json.left, 'right': json.right,
												'z-index': (json.layer + $scope.layer_main) 
											}, (json.speed * 1000), 
											function(){												
												var i = $scope.canvasData.order.length; while (i--){													 
													$scope.canvas.resizeCanvas(i, $scope.resolution_x, $scope.resolution_y);	
												}
												
												$scope.canvas.getCanvasDefaults(name, indexNum);
												$scope.canvas.applyCSSToCanvas(name, function(){});												

											});
								
						},
						
						organizeInfoDisplays:function(){
							
							var offsetLeft	= $('#' + $scope.activeCanvas).parent().offset().left, 
								offsetTop 	= $('#' + $scope.activeCanvas).parent().offset().top,
								height = $('#' + $scope.activeCanvas).height();							
														
							//$('#game-grid-bottom-info-b').css({"left": offsetLeft + 'px'})
							//$('#game-grid-bottom-info-c').css({"left": offsetLeft + 'px'})
							
														
						},
						
						keys:function(dir){
									  
								var speed = .5,
									name = $scope.activeCanvas,
									old = $scope.canvasData[name].setting;
								
								
								if ($scope.cameraType == "rotate"){
									 switch (dir){
										case 'u':
												newC = {
													p:  old.p,
													t:  speed,
													rX: (old.rX + 45), rY: old.rY, rZ: old.rZ,
													tX: old.tX, tY: old.tY, tZ: old.tZ,
													cX: old.cX, cY: old.cY, cZ: old.cZ,
													offsetX: old.oX, offsetY: old.oY,	
												};	
												
																			 
												$scope.io.addToControlBuffer([ "rtzCanvas", newC ], 1);
											break;
										case 'd':
												newC = {
													p:  old.p,
													t:  speed,
													rX: (old.rX - 45), rY: old.rY, rZ: old.rZ,
													tX: old.tX, tY: old.tY, tZ: old.tZ,
													cX: old.cX, cY: old.cY, cZ: old.cZ,
													offsetX: old.oX, offsetY: old.oY,	
												};								 
												$scope.io.addToControlBuffer([ "rtzCanvas", newC ], 1);
											break;
										case 'l':
												newC = {
													p:  old.p,
													t:  speed,
													rX: old.rX, rY: old.rY, rZ: (old.rZ + 45),
													tX: old.tX, tY: old.tY, tZ: old.tZ,
													cX: old.cX, cY: old.cY, cZ: old.cZ,
													offsetX: old.oX, offsetY: old.oY,	
												};									 
												$scope.io.addToControlBuffer([ "rtzCanvas", newC ], 1);
											break;
										case 'r':
												newC = {
													p:  old.p,
													t:  speed,
													rX: old.rX, rY: old.rY, rZ: (old.rZ - 45),
													tX: old.tX, tY: old.tY, tZ: old.tZ,
													cX: old.cX, cY: old.cY, cZ: old.cZ,
													offsetX: old.oX, offsetY: old.oY,	
												};									 
												$scope.io.addToControlBuffer([ "rtzCanvas", newC ], 1);
											break;
									}
								}	
								if ($scope.cameraType == "zoom"){
									 switch (dir){
										case 'u':
												newC = {
													p:  old.p,
													t:  speed,
													rX: old.rX, rY: old.rY, rZ: old.rZ,
													tX: old.tX, tY: old.tY, tZ: (old.tZ + 45),
													cX: old.cX, cY: old.cY, cZ: old.cZ,
													offsetX: old.oX, offsetY: old.oY,	
												};									 
												$scope.io.addToControlBuffer([ "rtzCanvas", newC ], 1);
											break;
										case 'd':
												newC = {
													p:  old.p,
													t:  speed,
													rX: old.rX, rY: old.rY, rZ: old.rZ,
													tX: old.tX, tY: old.tY, tZ: (old.tZ - 45),
													cX: old.cX, cY: old.cY, cZ: old.cZ,
													offsetX: old.oX, offsetY: old.oY,	
												};									 
												$scope.io.addToControlBuffer([ "rtzCanvas", newC ], 1);	
											break;
										case 'l':
												newC = {
													p:  old.p,
													t:  speed,
													rX: old.rX, rY: (old.rY + 45), rZ: old.rZ,
													tX: old.tX, tY: old.tY, tZ: old.tZ,
													cX: old.cX, cY: old.cY, cZ: old.cZ,
													offsetX: old.oX, offsetY: old.oY,	
												};									 
												$scope.io.addToControlBuffer([ "rtzCanvas", newC ], 1);
											break;
										case 'r':
												newC = {
													p:  old.p,
													t:  speed,
													rX: old.rX, rY: (old.rY - 45), rZ: old.rZ,
													tX: old.tX, tY: old.tY, tZ: old.tZ,
													cX: old.cX, cY: old.cY, cZ: old.cZ,
													offsetX: old.oX, offsetY: old.oY,	
												};									 
												$scope.io.addToControlBuffer([ "rtzCanvas", newC ], 1);
											break;
									}
								}
								if ($scope.cameraType == "translate"){
									 switch (dir){
										case 'u':
												newC = {
													p:  old.p,
													t:  speed,
													rX: old.rX, rY: old.rY, rZ: old.rZ,
													tX: old.tX, tY: (old.tY + 45), tZ: old.tZ,
													cX: old.cX, cY: (old.cY + 45), cZ: old.cZ,
													offsetX: old.oX, offsetY: old.oY,	
												};									 
												$scope.io.addToControlBuffer([ "rtzCanvas", newC ], 1);										
											break;
										case 'd':
												newC = {
													p:  old.p,
													t:  speed,
													rX: old.rX, rY: old.rY, rZ: old.rZ,
													tX: old.tX, tY: (old.tY - 45), tZ: old.tZ,
													cX: old.cX, cY: (old.cY - 45), cZ: old.cZ,
													offsetX: old.oX, offsetY: old.oY,	
												};									 
												$scope.io.addToControlBuffer([ "rtzCanvas", newC ], 1);		
											break;
										case 'l':
												newC = {
													p:  old.p,
													t:  speed,
													rX: old.rX, rY: old.rY, rZ: old.rZ,
													tX: (old.tX + 45), tY: old.tY, tZ: old.tZ,
													cX: (old.cX + 45), cY: old.cY, cZ: old.cZ,
													offsetX: old.oX, offsetY: old.oY,	
												};									 
												$scope.io.addToControlBuffer([ "rtzCanvas", newC ], 1);		
											break;
										case 'r':
												newC = {
													p:  old.p,
													t:  speed,
													rX: old.rX, rY: old.rY, rZ: old.rZ,
													tX: (old.tX - 45), tY: old.tY, tZ: old.tZ,
													cX: (old.cX - 45), cY: old.cY, cZ: old.cZ,
													offsetX: old.oX, offsetY: old.oY,	
												};									 
												$scope.io.addToControlBuffer([ "rtzCanvas", newC ], 1);	
											break;
									}
								}										
								if ($scope.cameraType == "perspective" && !$scope.togglePerspective){
									switch (dir){
										case 'u':
												newC = {
													p:  old.p + 25,
													t:  speed,
													rX: old.rX, rY: old.rY, rZ: old.rZ,
													tX: old.tX, tY: old.tY, tZ: old.tZ,
													cX: old.cX, cY: old.cY, cZ: old.cZ,
													offsetX: old.oX, offsetY: old.oY,	
												};									 
												$scope.io.addToControlBuffer([ "rtzCanvas", newC ], 1);	
												break;
										case 'd':
												newC = {
													p:  old.p - 25,
													t:  speed,
													rX: old.rX, rY: old.rY, rZ: old.rZ,
													tX: old.tX, tY: old.tY, tZ: old.tZ,
													cX: old.cX, cY: old.cY, cZ: old.cZ,
													offsetX: old.oX, offsetY: old.oY,	
												};									 
												$scope.io.addToControlBuffer([ "rtzCanvas", newC ], 1);	
												break;
									}
								}								
																					
						},
						
						applyCSSToCanvas:function(name, callback){
											
											settings = $scope.canvasData[name].setting;
											referenceValue = $scope.canvasData[name].referenceValue;
											
											
											var the_string = $scope.canvasInventory[referenceValue].cssName;
											var model = $parse( the_string );  // Get the model
											model.assign($scope, {
													'margin-left': settings.offsetX + 'px',
													'margin-top':  settings.offsetY + 'px',
													'transition':  settings.t + 's',
													'-webkit-transform-origin': settings.cX + 'px ' + (settings.cY - $scope.aspectFix) + 'px ' + settings.cZ + 'px',
													'-webkit-transform' : 'rotateX(' + settings.rX + 'deg) rotateY(' + settings.rY + 'deg) rotateZ(' + settings.rZ + 'deg)' +
																		  'translateX(' + settings.tX + 'px) translateY(' + settings.tY + 'px) translateZ(' + settings.tZ + 'px)',		
											});  

										callback();
						},
						
						getCanvasDefaults:function(name, refId){
							
								var  containerWidth  = $('#' + name).parent().width(),
									 containerHeight = $('#' + name).parent().height(),
									 referenceId = $('#' + name).attr('index'); 	
									 $('#' + name).css('transition', '0s');  // no delay for initial build
								
									settings = {
										p: 200,
										t:  0,
										rX: 0, rY: 0, rZ: 0,
										tX: 0, tY: 0, tZ: 0,
										cX: containerWidth/2, cY: containerHeight/2, cZ: 0,
										oX: 0, oY: 0,
										layer: 0								
									};


								$scope.canvasData[name].setting = settings;	
								$scope.canvasData[name].referenceValue = referenceId;
		
								$scope.camera_perspective_1 = 200;
								$scope.camera_perspective_2 = 200;
								$scope.camera_perspective_3 = 200;									


						},
						
						changeFocus:function(to){									
							$scope.activeCanvas = to;
							$scope.canvas.organizeInfoDisplays();						
						},
																	
						resizeCanvas:function(canvasId, resX, resY){	
							
								var name = $scope.canvasData.order[canvasId].name;
								if (  $scope.canvasData[name].render){
									
									var threeJS = $scope.canvasData[name].threeJS;												
									var camera = threeJS.camera;								 																																
									var canvas = $scope.canvasData.order[canvasId].object;
									
										var	aspectX = $(canvas).parent().width(),
											aspectY = $(canvas).parent().height();
		 									
										camera.aspect = aspectX/aspectY;
	
										var i = $scope.canvasData.order.length; while (i--){
							
											// get canvas object
											var canvasObj = $scope.canvasData.order[i].object;
			
												// fill canvas container
												$(canvasObj).width( '100%');
												$(canvasObj).height( $(canvasObj).width() * .5625 + 'px');
												
												$scope.aspectWidth =  $( window ).width() ;
												if ($scope.aspectWidth > $scope.aspectMaxWidth){
													$scope.aspectWidth = $scope.aspectMaxWidth; 
												}
												if ($scope.aspectWidth < $scope.aspectMinWidth){
													$scope.aspectWidth = $scope.aspectMinWidth;
												}
												$scope.aspectHeight = $scope.aspectWidth * .5625;
												
												
										}	
											
										camera.updateProjectionMatrix();	
								};	
										
						},

						changeResolution:function(type, callback){
							
							$scope.resolution_type = type;
							
							if (type == 'ultra'){
								$scope.resolution_x = 1920;
								$scope.resolution_y = 1080;								
							}
							if (type == 'high'){
								$scope.resolution_x = 1280;
								$scope.resolution_y = 720;
							}
							if (type == 'med'){
								$scope.resolution_x = 800;
								$scope.resolution_y = 450;
							}
							if (type == 'low'){
								$scope.resolution_x = 600;
								$scope.resolution_y = 337.5;
							}
							

							var i = $scope.canvasData.order.length; while (i--){
				
								// get canvas object
								var canvasObj = $scope.canvasData.order[i].object,
									preservedWidth = canvasObj.parent().width(),
									preservedHeight = canvasObj.parent().height(),
									name = $scope.canvasData.order[i].name; 
									
									// change render resolution
									renderer = $scope.canvasData[name].threeJS.renderer;
									renderer.setSize( $scope.resolution_x, $scope.resolution_y);

									// fill canvas container
									// fill canvas container
									$(canvasObj).width( '100%');
									$(canvasObj).height( $(canvasObj).width() * .5625 + 'px');
									
									$scope.aspectWidth =  $( window ).width() ;
									if ($scope.aspectWidth > $scope.aspectMaxWidth){
										$scope.aspectWidth = $scope.aspectMaxWidth; 
									}
									if ($scope.aspectWidth < $scope.aspectMinWidth){
										$scope.aspectWidth = $scope.aspectMinWidth;
									}											
									$scope.aspectHeight = $scope.aspectWidth * .5625;											
																					
							}	

							callback();
						},

						returnDefaultCanvas:function(canvasName){
							obj = { 
									usePostProcessing: true,
									status: 'default',
									render: false,
									canvas: {
										container: $('#' + canvasName),
									},
									scene: {
										scene: new THREE.Scene(),
										clock: new THREE.Clock(),
									},
									threeJS:{
										controls: {},
										renderer: {},
										camera: {},
										stats: {},
                                        allComponents: {},
										allParticles: {},
										allLights: {},
										allUniques: {},	
										allClickable: [],	
									},
									sceneComponents:{
										list: [],
                                        componentCount: 0,
										lightCount: 0,
										particleCount: 0,
									},
									filters:{
										render:  			{ enabled: true, parameters: null},
										copy: 				{ enabled: true, parameters: null},
										
										bloom:				{ enabled: false, parameters: { strength: 2, kernelSize: 25, sigma: 40.0, resolution: 256 } },
										bleach: 			{ enabled: false, parameters: { opacity: 2.0} },
										brightness:			{ enabled: false, parameters: { contrast: 0.8} },
										colorCorrection:	{ enabled: false, parameters: null },
										colorify: 			{ enabled: false, parameters: { color: 0xff0000} },										
																				
										film:				{ enabled: false, parameters: { sCount: 400, sIntensity: 0.9, nIntensity: 0.4} },
										dotScreen:			{ enabled: false, parameters: { scale: 2.5 } },
										glitch:				{ enabled: false, parameters: { dt_size: 64, goWild: false } },
										
										hueSaturation: 		{ enabled: false, parameters: {hue: 0.5, saturation: 0.5} },
										kaleido:			{ enabled: false, parameters: null },
										mirror:				{ enabled: false, parameters: null },
										
										luminosity: 		{ enabled: false, parameters: null },										
										RGBShift:			{ enabled: false, parameters: null },
										sepia:				{ enabled: false, parameters: null },
										vignette:	 		{ enabled: false, parameters: {darkness: 2.0} },
										
										horizontalBlur:		{ enabled: false, parameters: null },
										verticalBlur:	 	{ enabled: false, parameters: null },
										focus:				{ enabled: false, parameters: null },
										
									}
									
								 };
							return obj;
						},
						
						updateFilters:function(scene, camera, renderer, filters){
												var p;
												
												renderPass = new THREE.RenderPass( scene, camera, null, new THREE.Color().setHex( 0x000000 ), 0);
												copyPass = new THREE.ShaderPass( THREE.CopyShader );
											
												p = filters.bloom.parameters;
												bloomPass = new THREE.BloomPass(p.strength, p.kernelSize, p.sigma, p.resolution);
											
												p = filters.colorify.parameters;
												colorifyPass = new THREE.ShaderPass( THREE.ColorifyShader );
												colorifyPass.uniforms[ "color" ].value = new THREE.Color( p.color );
												
												p = filters.bleach.parameters;
												bleachPass = new THREE.ShaderPass( THREE.BleachBypassShader );
												bleachPass.uniforms[ "opacity" ].value = p.opacity;
								
												p = filters.brightness.parameters;
												brightnessContrastPass = new THREE.ShaderPass( THREE.BrightnessContrastShader );
												brightnessContrastPass.uniforms[ "contrast" ].value = p.contrast;
												
												colorCorrectionPass = new THREE.ShaderPass( THREE.ColorCorrectionShader );
								
												p = filters.film.parameters;
												filmPass = new THREE.ShaderPass( THREE.FilmShader );
												filmPass.uniforms[ "sCount" ].value = p.sCount;
												filmPass.uniforms[ "sIntensity" ].value = p.sIntensity;
												filmPass.uniforms[ "nIntensity" ].value = p.nIntensity;
												
												p = filters.glitch.parameters;
												glitchPass = new THREE.GlitchPass();
												glitchPass.goWild = p.goWild;
												
												p = filters.dotScreen.parameters;
												dotScreenPass = new THREE.ShaderPass( THREE.DotScreenShader );
												dotScreenPass.uniforms["scale"].value = p.scale;
								
												focusPass = new THREE.ShaderPass( THREE.FocusShader );
											
												horizontalBlurPass = new THREE.ShaderPass( THREE.HorizontalBlurShader );
												
												p = filters.hueSaturation.parameters;
												hueSaturationPass = new THREE.ShaderPass( THREE.HueSaturationShader );
												hueSaturationPass.uniforms[ "hue" ].value = p.hue;
												hueSaturationPass.uniforms[ "saturation" ].value = p.saturation;
								
												kaleidoPass = new THREE.ShaderPass( THREE.KaleidoShader );
								
												luminosityPass = new THREE.ShaderPass( THREE.LuminosityShader );
								
												mirrorPass = new THREE.ShaderPass( THREE.MirrorShader );
												
												RGBShiftPass = new THREE.ShaderPass( THREE.RGBShiftShader );
								
												sepiaPass = new THREE.ShaderPass( THREE.SepiaShader );
												
												verticalBlurPass = new THREE.ShaderPass( THREE.VerticalBlurShader );
												
												p = filters.vignette.parameters;
												vignettePass = new THREE.ShaderPass( THREE.VignetteShader );
												vignettePass.uniforms[ "darkness" ].value = p.darkness;
															
												composer = new THREE.EffectComposer( renderer);
											

												if(filters.render.enabled){composer.addPass( renderPass );};	
												if(filters.bloom.enabled){composer.addPass( bloomPass );};	
												if(filters.colorify.enabled){composer.addPass( colorifyPass );};	
												if(filters.bleach.enabled){composer.addPass( bleachPass );};	
												if(filters.brightness.enabled){composer.addPass( brightnessContrastPass );};
												if(filters.colorCorrection.enabled){composer.addPass( colorCorrectionPass );};
											
												if(filters.film.enabled){composer.addPass( filmPass );};	
												if(filters.dotScreen.enabled){composer.addPass( dotScreenPass );};
												if(filters.glitch.enabled){composer.addPass( glitchPass );};
												
												if(filters.focus.enabled){composer.addPass( focusPass );};
												if(filters.horizontalBlur.enabled){composer.addPass( horizontalBlurPass );};
												if(filters.hueSaturation.enabled){composer.addPass( hueSaturationPass );};
												if(filters.kaleido.enabled){composer.addPass( kaleidoPass );};
												if(filters.luminosity.enabled){composer.addPass( luminosityPass );};
												if(filters.mirror.enabled){composer.addPass( mirrorPass );};
												if(filters.RGBShift.enabled){composer.addPass( RGBShiftPass );};
												if(filters.sepia.enabled){composer.addPass( sepiaPass );};
												if(filters.verticalBlur.enabled){composer.addPass( verticalBlurPass );};
												if(filters.vignette.enabled){composer.addPass( vignettePass );};
											
												if(filters.copy.enabled){
													composer.addPass( copyPass );
													copyPass.renderToScreen = true;
												};	
												
												
												return composer;
						},
						
						returnTextureStandard:function(json){
									// texture / color
									var texture = null; 
									if( json.hasOwnProperty('texture') ){	
										texture = THREE.ImageUtils.loadTexture( $scope.pathing_textures + json.texture );
									}										
									
									// color	
									var color = null;
									if( json.hasOwnProperty('color') ){													
										if ( json.color == "random"){
											color = $scope.system.getRandomColor();		
										}					
										else{					
											color = json.color;
										}	
									}
									
									// material	
									var material = new THREE.MeshBasicMaterial( { wireframe: true } );
									if (texture != null){										
										material = new THREE.MeshLambertMaterial( { color: color, map: texture } );	
									};
									
									// repeatSet - primarily used for planes
									if (json.hasOwnProperty("repeatSet")){
										texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
										texture.repeat.set( json.repeatSet.x, json.repeatSet.y );
									}									
																		
											
									
									var _return = {
										texture: texture,
										color: color,
										material: material,										
									};
									
									return _return;
						},
						
						returnAssetStandard:function(object, json, geometry){

									object.defaults = {};

                                    // eventlogic
                                    function _eventLogic(){
                                        var events = [],
                                            listOfEvents = [];

                                        function execute(){
                                            for (var _exi = 0; _exi < events.length; _exi++){
                                                events[_exi].event()
                                            }
                                        }
                                        function add(content, name){
                                            if (name == undefined || name == null){
                                                name = "event_" + numberOfBehaviors()
                                            }
                                            content.name = name;
                                            events.push(content);
                                            listOfEvents.push(name);
                                        }
                                        function removeByName(nameArray){

                                            var noMatch = true;
                                            var m = nameArray.length; while(m--) {
                                                name = nameArray[m];
                                                // delete from array
                                                var i = listOfEvents.length;
                                                while (i--) {
                                                    if (name == listOfEvents[i]) {
                                                        listOfEvents.splice(i, 1);  // remove from list
                                                        events.splice(i, 1);        // remove from object
                                                        noMatch = false;
                                                    }
                                                }
                                                ;
                                            }

                                            if (noMatch){
                                                console.log("That name does not exist.\nHere is a list of existing behaviors:\n" + view().listOfEvents)
                                            }


                                        }

                                        function numberOfBehaviors(){
                                            return listOfEvents.length;
                                        }
                                        function view(){
                                            _return = {
                                                events: events,
                                                listOfEvents: listOfEvents
                                            }
                                            return _return;
                                        }

                                        return{
                                            viewList:view,
                                            addBehavior:add,
                                            removeByName:removeByName,
                                            removeByIndex:removeByName,
                                            execute: execute
                                        }
                                    };

									// position
									if( json.position.hasOwnProperty('random') && json.position.random){
										var rx = $scope.system.returnRandomNumber(json.position.x, true),
											ry = $scope.system.returnRandomNumber(json.position.y - json.dimensions.y, false),
											rz = $scope.system.returnRandomNumber(json.position.z, true);
																					
										object.position.set(rx, ry, rz);	
										object.defaults.position = {x: rx, y: ry, z: rz};	
																
									}
									else{
										
										object.position.set(json.position.x, json.position.y, json.position.z);
										object.defaults.position = {x: json.position.x, y: json.position.y, z: json.position.z};	
									}
									
									// rotation	
									if( json.hasOwnProperty('rotation') ){
										if( json.rotation.hasOwnProperty('random') && json.rotation.random){
											var rx = $scope.system.returnRandomNumber(360, true),
												ry = $scope.system.returnRandomNumber(360, true),
												rz = $scope.system.returnRandomNumber(360, true);
											
											object.rotation.set(rx * Math.PI / 180, ry * Math.PI / 180, rz * Math.PI / 180);	
											object.defaults.rotation = {x: rx, y: ry, z: rz};									
										}
										else{
											object.rotation.set(json.rotation.x * Math.PI / 180, json.rotation.y * Math.PI / 180, json.rotation.z * Math.PI / 180);
											object.defaults.rotation = {x: json.rotation.x, y: json.rotation.y, z: json.rotation.z}	;	
										}
									}
									else{
											object.rotation.set(0, 0, 0);
											object.defaults.rotation = {x: 0, y: 0, z: 0}	;		
									}
																					
									// tween 
									object.tweenData = {
										tween: null
									}; 
									
									// auto animation
									if( json.hasOwnProperty('animation') ){
										object.animation = json.animation;
									}
									else{
										object.animation = {
											enabled: false,
								            rotationSpd: {x: 0, 	y: 0.0, 	z: 0}      
								        }
									}

									// set shadows - true by default
									object.castShadow = true; object.receiveShadow = true;
									if( json.hasOwnProperty('shadows') ){
										if( json.shadows.hasOwnProperty('cast') )	{	object.castShadow = json.shadows.cast;  		}
										if( json.shadows.hasOwnProperty('recieve') ){	object.receiveShadow = json.shadows.recieve; 	}										
									}
									
									// isClickable - true by default
									object.isClickable = true;
									if( json.hasOwnProperty('isClickable') && !json.isClickable){
										object.isClickable = false;
									};
									
									// outline
									outlineMesh = null;
									if( json.hasOwnProperty('outline') ){
										if(json.outline.active){
											var outlineMaterial = new THREE.MeshBasicMaterial( { color: json.outline.color, side: THREE.BackSide } );
											outlineMesh = new THREE.Mesh( geometry, outlineMaterial );
											outlineMesh.position = object.position;
											outlineMesh.rotation.set(object.rotation.x, object.rotation.y, object.rotation.z);
											outlineMesh.scale.dynamic = true;
											outlineMesh.scale.verticesNeedUpdate =true;


                                            outlineMesh.animation = json.animation;
                                            // event
                                            outlineMesh._events = {
                                                onClick: new _eventLogic(),
                                                onHover: new _eventLogic(),
                                                onLoop: new _eventLogic()
                                            }

                                            var self = outlineMesh;
                                            // bind copy rotation/dimensions to object
                                            console.log(self)
                                            event =   { event: function(){

                                                self.rotation.set(object.rotation.x, object.rotation.y, object.rotation.z);
                                                self.position = object.position;
                                            }};
                                            outlineMesh._events.onLoop.addBehavior(event);

										}
									}

                                    // event
                                    object._events = {
                                        onClick: new _eventLogic(),
                                        onHover: new _eventLogic(),
                                        onLoop: new _eventLogic()
                                    }

                                    // copy behavior event

                                    // bind event to onLoop example
                                    if ($scope.doOnce == undefined) {
                                        $scope.doOnce = true;
                                        event = { event: function () {
                                            object.rotation.x += .02
                                        }};
                                        object._events.onLoop.addBehavior(event);
                                    }


                                    // bind event onClick
                                    event =   { event:  $scope.events.selectOnClick( {component: object, outlineMesh: outlineMesh}  ) };
                                    object._events.onClick.addBehavior(event);



									_return = {
										object: object,
										outlineMesh: outlineMesh,
									};

									return _return;
									
									
						},
						
						assetCreator:{

							makeRenderer:function(_renderer, canvas, callback){
								var renderer = new THREE.WebGLRenderer({canvas: canvas.get(0), alpha: true, antialias: true });
									if (_renderer.solidBG){
										renderer.setClearColor( _renderer.bgColor, _renderer.bgOpacity );
									}
									renderer.shadowMapEnabled = true;
								callback(renderer);
							},
							
							makeCamera:function(_camera, callback){
								var camera = new THREE.PerspectiveCamera( _camera.fov, 0, _camera.near, _camera.far);
								camera.position.set( _camera.position.x, _camera.position.y, _camera.position.z );
								camera.rotation.set( _camera.rotation.x * Math.PI / 180, _camera.rotation.y * Math.PI / 180, _camera.rotation.z* Math.PI / 180 );
								
								// defaults	
								camera.defaults = {
									position: {x: camera.position.x, y: camera.position.y, z: camera.position.z},
									rotation: {x:  _camera.rotation.x, y:  _camera.rotation.y, z:  _camera.rotation.z}
								};	

								// tween and animations
								camera.tweenData = {
									tween: null
								}; 								
								
								callback( camera );
							},
																					
							makeStats:function(_stats, canvasId, callback){
								if (_stats.show){
									var stats = new Stats(canvasId);
									stats.setMode(0);
									$(_stats.domEle).append( stats.domElement );
									callback(stats);
								}
							},

							makeLight:function(_light, baseNum, allUniques, callback){

								var returnSet = [],
									nametype = "allLights";
								
								var i = _light.data.length; while(i--){
									obj = _light.data[i];
									
									if ( obj.enable == true ){
										var lightTarget = null;
										
										
										if (obj.type == "Ambient"){
											var light = new THREE.DirectionalLight( obj.color );
										} 		
										if (obj.type == "SpotLight"){ 
											var light = new THREE.SpotLight( obj.color );	
											
											// spotlights need a target to follow (they don't rotate)
											var lightTarget = new THREE.Object3D(),
												px = 200, 	py = 0, 	pz = 0,
												rx = 0, 	ry = 0, 	rz = 0;
												
												lightTarget.defaults = {};
												lightTarget.defaults.position = {x: px, y: py, z: pz};		
												lightTarget.defaults.rotation = {x: rx, y: ry, z: rz};												
												lightTarget.position.set(px, py, pz);

												lightTarget.tweenData = {
													tween: null
												};
												 													
												light.target = lightTarget;	
																										
										}
										if (obj.type == "Area"){
											var light = new THREE.DirectionalLight( obj.color );
										} 
										
										// defaults
										light.defaults = {};	
										
										// position
										if( obj.position.hasOwnProperty('random') && obj.position.random){
											var rx = $scope.system.returnRandomNumber(obj.position.x, true),
												ry = $scope.system.returnRandomNumber(obj.position.y - obj.dimensions.y, false),
												rz = $scope.system.returnRandomNumber(obj.position.z, true);
																						
											light.position.set(rx, ry, rz);	
											light.defaults.position = {x: rx, y: ry, z: rz};	
																	
										}
										else{
											light.position.set(obj.position.x, obj.position.y, obj.position.z);
											light.defaults.position = {x: obj.position.x, y: obj.position.y, z: obj.position.z};	
										}
										
										
										// rotation	
										if( obj.rotation.hasOwnProperty('random') && obj.rotation.random){
											var rx = $scope.system.returnRandomNumber(360, true),
												ry = $scope.system.returnRandomNumber(360, true),
												rz = $scope.system.returnRandomNumber(360, true);
											
											light.rotation.set(rx * Math.PI / 180, ry * Math.PI / 180, rz * Math.PI / 180);	
											light.defaults.rotation = {x: rx, y: ry, z: rz};									
										}
										else{
											light.rotation.set(45 * Math.PI / 180, 45 * Math.PI / 180, 45 * Math.PI / 180);
											light.defaults.rotation = {x: obj.rotation.x, y: obj.rotation.y, z: obj.rotation.z}	;	
										}
										
										// assign name/type
										light.type = nametype;
										light.name = "_" + (i + baseNum);
									
															// tweenData
										light.tweenData = {
											tween: null
										}; 
										
	
										// shadowing 
										if(obj.shadow.enabled){
											light.shadowCameraVisible = obj.shadow.shadowCameraVisible;
											light.shadowDarkness = obj.shadow.shadowDarkness;
											light.intensity = obj.shadow.intensity;
											light.castShadow = true;	
										}	
										
																		
										if ( obj.hasOwnProperty('unique') ){
											allUniques[obj.unique] = {component: light, target: lightTarget};
										};	
										returnSet.push({component: light, target: lightTarget});
										
									}
										
									
								}
								
								callback(returnSet);
								
							},

							makePrimatives:function(_obj, threeJS, type, callback){

								var returnSet = [],
                                    allUniques = threeJS.allUniques,
                                    allComponents = threeJS.allComponents,
                                    allClickable = threeJS.allClickable,
                                    total = Object.keys(threeJS.allComponents).length;

								var i = _obj.data.length; while(i--){
									obj = _obj.data[i];

                                    // texture
                                    var returned = $scope.canvas.returnTextureStandard(obj),
                                        material = returned.material;
                                    texture = returned.texture;

									// type
                                    var primative;
                                    if (type == "cube") {
                                        primative = new THREE.BoxGeometry(obj.dimensions.x, obj.dimensions.y, obj.dimensions.z);
                                    }
                                    if (type == "sphere") {
                                        primative = new THREE.SphereGeometry(obj.dimensions.x, obj.dimensions.y, obj.dimensions.z);
                                    }
                                    if (type == "plane") {
                                        primative = new THREE.PlaneGeometry(obj.dimensions.width, obj.dimensions.height, obj.dimensions.widthSegment, obj.dimensions.heightSegment);
                                        material = new THREE.MeshLambertMaterial( { map: texture, side: THREE.DoubleSide } );
                                    }

									// component				
									var component = new THREE.Mesh( primative, material );

									// set defaults
									$scope.canvas.returnAssetStandard(component, obj, primative);

                                    // add to all components
                                    component.catagory = "allComponents";
                                    component.type = type;
                                    component.name = "_" + (i + total);
                                    allComponents[ component.name ] = {component: component, outline: outlineMesh};



                                    // add to clickable
                                    if (component.isClickable) {
                                        allClickable.push(component);
                                    }

									// push to array
									returnSet.push({component: component, outline: outlineMesh});
	
								}

																
								callback(returnSet);

								
							},
							
							makeParticles:function(_particles, baseNum, allUniques, callback ){

								var returnSet = [];
								var i = _particles.data.length; while(i--){
									obj = _particles.data[i]; 
										
										var particles = new THREE.Geometry;
										var p = obj.amount; while (p--){
											if (obj.type.is == "texture"){
												var particleTexture = THREE.ImageUtils.loadTexture($scope.pathing_particle + '/' + obj.type.image);
											}																	
										    var particle = new THREE.Vector3(Math.random() * obj.range.x - obj.range.x/2 , Math.random() * obj.range.ceiling, Math.random() * obj.range.z - obj.range.z/2) ;										     	
										    particle.velocity = new THREE.Vector3(Math.random() * obj.physics.velocitySpd.x, Math.random() * obj.physics.velocitySpd.y, Math.random() * obj.physics.velocitySpd.z);
										    particle.velocitySpd = obj.physics.velocitySpd;
 								
										  	particles.vertices.push(particle);
										  										   										   											
										}
										var particleSize = obj.size.base + Math.round((Math.random() * obj.size.variation));
									    var particleMaterial = new THREE.ParticleBasicMaterial({ 
											  color: 0xFFFFFF,
											  size: particleSize,
											  map: particleTexture,
											  blending: THREE.AdditiveBlending,
											  transparent : true,
											  depth_test: false
									    });											

										// particle system attributes
										particleSystem = new THREE.ParticleSystem(particles, particleMaterial);
										
										// name/group
										particleSystem.type = "allParticles";
										particleSystem.name = "_" + (i + baseNum);
										
										// attributes
										particleSystem.sortParticles = true;
										particleSystem.frustumCulled = true;
										
										// range
										particleSystem.range = obj.range;
										
										// animation
										particleSystem.animation = obj.animation;

										// physics										
										particleSystem.physics = obj.physics;
										
										
										
										returnSet.push({particleSystem: particleSystem});
								}
								callback(returnSet);	
							},
													
							makeSkybox:function(_skybox, callback){
								var returnSet = [];
								var i = _skybox.data.length; while(i--){	
									obj = _skybox.data[i]; 						
									
									var imagePrefix = $scope.pathing_skybox + obj.package + "/";
									var directions  = ["xpos", "xneg", "ypos", "yneg", "zpos", "zneg"];
									var imageSuffix = ".png";
									var skyBoxGeometry = new THREE.BoxGeometry( obj.dimensions.x, obj.dimensions.y, obj.dimensions.z );		
									
									var materialArray = [];
									for (var n = 0; n < 6; n++){
										materialArray.push( new THREE.MeshBasicMaterial(
											{
											map: THREE.ImageUtils.loadTexture( imagePrefix + directions[n] + imageSuffix ),
											side: THREE.BackSide
											}
										));
									};
															
									
									var skyBoxMaterial = new THREE.MeshFaceMaterial( materialArray );
									var skyBox = new THREE.Mesh( skyBoxGeometry, skyBoxMaterial );
									
									returnSet.push({skybox: skyBox});
								};
								
								callback(returnSet);
							},

						},

						addToScene:function(sceneJson, container, addImmediately, callback){
			
								$scope.load.get_json( sceneJson, function(isValid, json){
									
									if (isValid){
										$scope.canvas.build( container, addImmediately, json );
										callback();
									}
									else{
										alert("JSON failed to load.");
									}
								});
						},

						clearScene:function(sceneName, callback){
										
								// variables needed
								var allAssets = [],
									scene =  $scope.canvasData[sceneName].scene.scene,																 
									list = $scope.canvasData[sceneName].sceneComponents.list,	
									threeJS = $scope.canvasData[sceneName].threeJS, 
									renderer = threeJS.renderer,
									camera = threeJS.camera;
								 										
								
								// get list of all objects	
								var n = list.length; while(n--){
										allAssets.push([scene, list[n]]);
								};	

								// clear list			
								var i = allAssets.length; while(i--){
									var scene = allAssets[i][0];
									var object = allAssets[i][1];
									scene.remove(object);		
								}	
								
								// render empty screen
								renderer.render(scene, camera);	
	
								
								// remove from array
								var i = $scope.canvasData.order.length; while(i--){
									 var aName = $scope.canvasData.order[i].name;
									
									 if (aName == sceneName){
									 	$scope.canvasData.order.splice(i, 1);
									 	$('#stats_' + sceneName).remove();
									 }
								}
								
								// reset default canvas info	
								$scope.canvasData[sceneName] = $scope.canvas.returnDefaultCanvas(sceneName);
								
								callback();		

						},
								
						buildCanvasContainers:function(buildNum){
							
							var parentId = "game-grid-control_" + buildNum,
								canvasId = "canvasContainer_" + buildNum,
								cssId = "myCss_" + buildNum;
							
							var content = '<div id="' + parentId + '" ng-mouseover="canvas.changeFocus(\'' + canvasId + '\')" ng-class="{\'' + canvasId + '\': \'active-outline\'}[activeCanvas]"  class="gameContainer_shell" ng-style="{\'-webkit-perspective\': camera_perspective_1 + \'px\'}"  >';					
								content += '<canvas index="' + buildNum + '"  id="' + canvasId + '" class="gameCanvas"   ng-style="' + cssId + '" ng-class="{false: \'fadeOut\', true: \'fadeIn\'}[loadReady]"></canvas>';
								content += '<div class="gameMask"></div>';							
								content += '</div>';											
												
							$scope.canvasInventory.push({parentName: parentId, canvasName: canvasId, cssName: cssId });				
							
							
							$scope.system.injectAngular(content, '#canvasContainers');
							
							
						},

						buildCanvasInit:function( buildTotal ){
							
							for (i = 0; i < buildTotal; i++){
								$scope.canvas.buildCanvasContainers(i);
							}
				
							$('.gameCanvas').each(function(index){
										var container = $(this),
											canvasName = container.attr('id');
											container.parent().css('width', container.width); 
											container.parent().css('height', container.height); 
																			
											$scope.canvasData[container.attr('id')] = $scope.canvas.returnDefaultCanvas(container.attr('id'));
											$(container).click(function(event){
												$scope.canvas.clickOn(event);
											});												
											
							});
						},

						build:function(canvasId, addImmediately, data){
										
										var container = $scope.canvasData[canvasId].canvas.container,
											scene = $scope.canvasData[canvasId].scene.scene,
											name = container.attr('id'),
											threeJS = $scope.canvasData[name].threeJS,
												allUniques = threeJS.allUniques,

											sceneComponents = $scope.canvasData[name].sceneComponents,
												list = sceneComponents.list;

                                        // assit functions
                                        var createAsset = function(data, type){
                                            $scope.canvas.assetCreator.makePrimatives(data, threeJS, type, function(returnSet){
                                                var i = returnSet.length; while(i--){

                                                    list.push(returnSet[i].component);

                                                    if ( addImmediately){
                                                        scene.add(returnSet[i].component);
                                                    };

                                                    if (returnSet[i].outline != undefined || returnSet[i].outline != null){
                                                        list.push(returnSet[i].outline);
                                                        if ( addImmediately){
                                                            scene.add(returnSet[i].outline);
                                                        }
                                                    };
                                                }
                                            });
                                        }

                                        var updateCount = function(){
                                            sceneComponents.lightCount = Object.keys(threeJS.allLights).length - 1;
                                            if ( sceneComponents.lightCount < 0){sceneComponents.lightCount = 0;}

                                            sceneComponents.particleCount = Object.keys(threeJS.allParticles).length - 1;
                                            if ( sceneComponents.particleCount < 0){sceneComponents.particleCount = 0;}

                                            sceneComponents.componentCount = Object.keys(threeJS.allComponents).length -1;
                                            if ( sceneComponents.componentCount < 0){sceneComponents.componentCount = 0;}
                                        }
											
										// scene
										if (data.hasOwnProperty('scene')){
											var _scene = data.scene;
											if (_scene.fps != undefined){
												scene.fps = _scene.fps;
											}
											else{
												scene.fps = 60;
											};
											
											if (_scene.fog.enabled){									
												fog = new THREE.Fog( _scene.fog.color, _scene.fog.near, _scene.fog.far );
												scene.fog = fog; 
											};
											
											// axis
											if (_scene.axesHelper.enabled){
												var axes = new THREE.AxisHelper(_scene.axesHelper.length);
												list.push(axes);
											};
											
										}
											
										// renderer
										if (data.hasOwnProperty('renderer')){
											var _renderer = data.renderer;
											$scope.canvas.assetCreator.makeRenderer(_renderer, container, function(data){
												threeJS.renderer = data;											
											});
										}

										// setup camera
										if (data.hasOwnProperty('camera')){
											var _camera = data.camera;
											var camera = null;
											$scope.canvas.assetCreator.makeCamera(_camera, function(data){
												threeJS.camera = data;
												camera = data;
												list.push(camera);
											});
										}

										// mouse
										if (data.hasOwnProperty('mouse') && camera != null){
												var _mouse = data.mouse;
												if (_mouse.controls){
													threeJS.controls = new THREE.OrbitControls( camera, _renderer.domElement );
												}	
											}

										// stats
										if (data.hasOwnProperty('stats')){
												var _stats = data.stats;
												$scope.canvas.assetCreator.makeStats(_stats, canvasId, function(data){
													threeJS.stats = data;
												});
											}

										// build lights
										if (data.hasOwnProperty('light')){
											var _light = data.light;
											$scope.canvas.assetCreator.makeLight(_light,  sceneComponents.lightCount, allUniques, function(returnSet){
												var i = returnSet.length; while(i--){	
													list.push(returnSet[i].component);
													//threeJS.allClickable.push(returnSet[i].component);																
													threeJS.allLights[ returnSet[i].component.name ] = returnSet[i]	;	
													
													if (addImmediately){
														scene.add(returnSet[i].component);
													}
																																
													if (returnSet[i].outline != undefined || returnSet[i].outline != null){	
														list.push(returnSet[i].outline);
														
														if ( addImmediately){
															scene.add(returnSet[i].outline);
														}
														
													};
											
												}				
												threeJS.allLights.total = Object.keys(threeJS.allLights).length;													
											});
										};	

										// particles
										if (data.hasOwnProperty('particles')){
											var _particles = data.particles;
											$scope.canvas.assetCreator.makeParticles(_particles, sceneComponents.particleCount, allUniques, function(returnSet){
												var i = returnSet.length; while(i--){		
													
													threeJS.allParticles[ returnSet[i].particleSystem.name ] = returnSet[i]	;			
													list.push(returnSet[i].particleSystem);
													
													if (addImmediately){														
														scene.add(returnSet[i].particleSystem);
													}
													
												};
											
												threeJS.allParticles.total =  Object.keys(threeJS.allParticles).length;
												
												
											});
										}

										// build skybox
										var skybox = null;										
										if (data.hasOwnProperty('skybox')){
											var _skybox = data.skybox;
											$scope.canvas.assetCreator.makeSkybox(_skybox, function(returnSet){
												var i = returnSet.length; while(i--){
													list.push(returnSet[i].skybox);	
													if (addImmediately){														
														scene.add(returnSet[i].skybox);
													}													
												};
											});
											 
										};


                                        // build primatives
                                        var primativeTypes = ['cube', 'sphere', 'plane']
                                        var p = primativeTypes.length; while (p--) {
                                            type = primativeTypes[p]
                                            if (data.hasOwnProperty(type)) {
                                                createAsset(data[type], type);
                                            }
                                            ;
                                        }
										

                                        updateCount();
										if (!addImmediately){
											$scope.canvasData.order.push({name: name, object: container});
											$scope.canvasData[name].status = "ready";	
											$scope.canvasData[name].render = true;	
										}
										
						},

						constructScene:function(sceneName, callback){
								$scope.assetsLoaded = 0; $scope.totalAssets = 0;
								var allAssets = [];
									threeJS = $scope.canvasData[sceneName].threeJS, 
								 	scene =  $scope.canvasData[sceneName].scene.scene,																 
									list = $scope.canvasData[sceneName].sceneComponents.list;										
									n = list.length; while(n--){
										allAssets.push([scene, list[n]]);
									};	
							
								
								$scope.totalAssets = allAssets.length; 
								var i = allAssets.length; while(i--){
									var scene = allAssets[i][0];
									var object = allAssets[i][1];
									scene.add(object);
									$timeout(function(){
										$scope.assetsLoaded ++;	
										if ( $scope.assetsLoaded == $scope.totalAssets){											
											callback();
										};
									}, i * 10);	// increase for artifical loading speed					
								}
	
						},

						constructAllScenes:function(callback){
								$scope.assetsLoaded = 0; $scope.totalAssets = 0;
								
								var allAssets = [];
								var i = $scope.canvasData.order.length; while (i--){	
										var	name = $scope.canvasData.order[i].name, 
											filters = $scope.canvasData[name].filters, 
											threeJS = $scope.canvasData[name].threeJS, 
										 	scene =  $scope.canvasData[name].scene.scene,	
										 	renderer = threeJS.renderer,
										 	camera = threeJS.camera,															 
											list = $scope.canvasData[name].sceneComponents.list;	

											
											//CREATE INITIAL FILTERS
											composer = $scope.canvas.updateFilters(scene, camera, renderer, filters);
												
										//	$scope.canvasData['canvasContainer_1'].composer = composer;
											$scope.canvasData[name].composer = composer;
																				
											n = list.length; while(n--){
												allAssets.push([scene, list[n]]);
											};	
			
								};
								
							
								
								
								$scope.totalAssets = allAssets.length; 
								var i = allAssets.length; while(i--){
									var scene = allAssets[i][0];
									var object = allAssets[i][1];
									scene.add(object);
									$timeout(function(){
										$scope.assetsLoaded ++;	
										if ( $scope.assetsLoaded == $scope.totalAssets){											
											callback();
										};
									}, i * 100);	// increase for artifical loading speed					
								}
	
						},
							
						clickOn:function(evt){
								evt.preventDefault();
										var obj = $scope.canvas.intersectCheck();
                                            if (obj != false){
                                                obj.component._events.onClick.execute();
                                            }
										/*
										if (obj != false){
											obj.component.animation.enabled = !obj.component.animation.enabled;
											obj.component.animation.rotationSpd = {x: .025, y: .025, z: .025};
										}
										*/
								
						},
						
						intersectCheck: function(callback){
								if ($scope.activeCanvas != undefined || $scope.activeCanvas != null){
									//setup raycast
									var projector = new THREE.Projector(), 
								    mouse_vector = new THREE.Vector3(),
								    ray = new THREE.Raycaster( new THREE.Vector3(0,0,0), new THREE.Vector3(0,0,0) ),
								    intersects = []; 											
									
									// grab canvas information
									var render = $scope.canvasData[$scope.activeCanvas].render;
									if (render){					
										var threeJS = $scope.canvasData[$scope.activeCanvas].threeJS,
											camera = threeJS.camera,
											container = threeJS.container,
											allClickable = threeJS.allClickable;
											
									   	// get mouse coords								
										var cords = $scope.system.getMouseVector(container); 

										// new vector
									    mouse_vector.set( cords.x, cords.y, 1 );
									    
										// update and find intersects
									    projector.unprojectVector( mouse_vector, camera );
									    var direction = mouse_vector.sub( camera.position ).normalize();							  
									    ray.set( camera.position, direction );
									    intersects = ray.intersectObjects( allClickable );

		
									    if( intersects.length ) {
									       var 	catagory = intersects[0].object.catagory,
									       		name = intersects[0].object.name;
									      		return threeJS[catagory][name];
									    }
									    else{
									    	return false;
									    }
								   }
							   }
							   else{ return false; }
						},							

						pauseClocks:function(){
								$scope.isBrowserActive = false;							
								$scope.globalClock.stop();
								var i = $scope.canvasData.order.length; while (i--){	
										var name = $scope.canvasData.order[i].name; 
											clock = $scope.canvasData[name].scene.clock;
											clock.stop();
											
								}							
						},
						
						resumeClocks:function(){	
								$scope.isBrowserActive = true;
								$scope.globalClock.start();
								var i = $scope.canvasData.order.length; while (i--){	
										var name = $scope.canvasData.order[i].name; 
											clock = $scope.canvasData[name].scene.clock;
											clock.stop();
								}	
						},

						animateLoop:function(){
							
								var oldSettings,
									oldMaterial,
									scaleSize = 1.15,
									hoverColor = 0x00FF00;
									
								
								function renderLoop() {	
									
									function postProcessingHack(){
										
											THREE.EffectComposer.camera = new THREE.OrthographicCamera( -1, 1, 1, -1, 0, 1 );
											THREE.EffectComposer.quad = new THREE.Mesh( new THREE.PlaneGeometry( 2, 2 ), null );
											THREE.EffectComposer.scene = new THREE.Scene();
											THREE.EffectComposer.scene.add( THREE.EffectComposer.quad );
											
										
									}
																		 
									function applyPhysics(obj, range, physics){
										var velocity = obj.velocity,
											velocitySpd = obj.velocitySpd,
											rotationSpd = physics.rotationSpd,
											velocityDefaults = physics.velocitySpd,
											maxVelocity = physics.maxVelocity,	
											attributes = physics.attributes,
											windVector = physics.windVector; 	
																		
						

										// establish maximum velocity									
										if (velocity.y < 0){						    															    		
											if (velocity.y < -maxVelocity.y){ velocity.y = -maxVelocity.y;};
										}
										if (velocity.y > 0){						    		
											if (velocity.y > maxVelocity.y){ velocity.y = maxVelocity.y;};
										}						
										if (velocity.x > 0){
											if (velocity.x > maxVelocity.x){ velocity.x = maxVelocity.x;};
										}
										if (velocity.x < 0){
											if (velocity.x < -maxVelocity.x){ velocity.x = -maxVelocity.x;};									
										}	
										if (velocity.z > 0){
											if (velocity.z > maxVelocity.z){ velocity.z = maxVelocity.z;};
										}
										if (velocity.x < 0){
											if (velocity.z < -maxVelocity.z){ velocity.z = -maxVelocity.z;};									
										}	
										
										if (windVector.x > 0){
											if (windVector.x > maxVelocity.x){ windVector.x = maxVelocity.x;};
										}
										if (windVector.x < 0){
											if (windVector.z < -maxVelocity.z){ windVector.x = -maxVelocity.x;};									
										}	
										if (windVector.z > 0){
											if (windVector.x > maxVelocity.z){ windVector.z = maxVelocity.z;};
										}
										if (windVector.z < 0){
											if (windVector.z < -maxVelocity.z){ windVector.z = -maxVelocity.z;};									
										}	
										
										// get gravity speed
										var gravitySpd = $scope.globalPhysics.gravity.y;
	
																																
				
										// apply gravity and velocity to object
										isEffected = true;
										if ( (obj.y == range.floor && $scope.globalPhysics.gravity.y > 0) 	|| 
											 (obj.y == range.ceiling && $scope.globalPhysics.gravity.y < 0)	){
											isEffected = false;
										}
										if (isEffected){
											
											velocity.y = velocity.y -= (velocitySpd.y).toFixed(2) * gravitySpd;
										}
										
										if(physics.jitter !== false && $scope.globalPhysics.gravity.y == 0){											
											velocitySpd.y = Math.random() < 0.5 ? physics.jitter*.01 : -physics.jitter*.01;	
										}																															
										
										// global wind if not on ground or ceiling
										if (obj.y > range.floor && obj.y < range.ceiling){																						
											windVector.x = $scope.globalPhysics.gravity.x ;
											windVector.z = $scope.globalPhysics.gravity.z ;
											velocity.x = velocity.x -= (velocitySpd.x + windVector.x) ;
											velocity.z = velocity.z -= (velocitySpd.z + windVector.z) ;
																																		
										}
										
										 
										// attributes
										var impactY = false,
											impactX = false,	
											impactZ = false;
											
																		

										if(attributes.walls.y){	
											if( obj.y < range.floor){
												obj.y = range.floor;
												impactY = true;
											};
											if( obj.y > range.ceiling){
												obj.y = range.ceiling;
												impactY = true;																									
											}
										}	
										else{
											if( obj.y < range.floor){
												obj.y = range.ceiling;
												
											}
											if( obj.y > range.ceiling){
												obj.y = range.floor;
											}
										}
										
										if( attributes.walls.x){

											if( obj.x > range.x && velocity.x > 0 ){
												obj.x = range.x;
												impactX = true;																									
											}
											if( obj.x < -range.x && velocity.x < 0 ){
												obj.x = -range.x;
												impactX = true;																									
											}													
										}
										else{
											if( obj.x < -range.x){
												obj.x = range.x;
												
											}
											if( obj.x > range.x){
												obj.x = -range.x;
												
											}
										}
										
										if(attributes.walls.z){

											if( obj.z > range.z && velocity.z > 0 ){
												obj.z = range.z;
												impactZ = true;																									
											}
											if( obj.z < -range.z && velocity.z < 0 ){
												obj.z = -range.z;
												impactZ = true;																									
											}													
										}
										else{
											if( obj.z < -range.z){
												obj.z = range.z;
												
											}
											if( obj.z > range.z){
												obj.z = -range.z;
												
											}
										}												
									

																	
										// touch floor/ceiling			
										if (impactY){	
											
											if(velocity.y > 0 && Math.floor(Math.abs(velocity.y)) < 3){ velocity.y = 0; };
											if(velocity.y < 0 && Math.floor(Math.abs(velocity.y)) < 3){ velocity.y = 0; };
											if( attributes.friction > 0){
												velocitySpd.x = velocitySpd.x/attributes.weight;												
												velocitySpd.z = velocitySpd.z/attributes.weight;
											}
											
											velocity.y = -velocity.y/(attributes.weight + Math.random(attributes.weight));											
										};
										
										if (impactX){
												
												if(velocity.x > 0 && Math.floor(Math.abs(velocity.x)) < 3){ velocity.x = 0; };
												if(velocity.x < 0 && Math.floor(Math.abs(velocity.x)) < 3){ velocity.x = 0; };
												velocity.x = -velocity.x - Math.random(attributes.weight);
												
										};
										
										if (impactZ){
												
												if(velocity.z > 0 && Math.floor(Math.abs(velocity.z)) < 3){ velocity.z = 0; };
												if(velocity.z < 0 && Math.floor(Math.abs(velocity.z)) < 3){ velocity.z = 0; };
												velocity.z = -velocity.z - Math.random(attributes.weight);
												
										};										
										
										// if touching floor or ceiling, apply friction
										if ( (obj.y == range.floor || obj.y == range.ceiling) && attributes.friction > 0 ){
											windVector.x = 0;
											windVector.z = 0;
											velocity.x = velocity.x/(attributes.friction + 1);
											velocity.z = velocity.z/(attributes.friction + 1);
											if ( Math.abs(velocity.x) < .5){ velocity.x = 0;}
											if ( Math.abs(velocity.z) < .5){ velocity.z = 0;}
										}
										
										
										obj.y += velocity.y; 
										obj.x += velocity.x + windVector.x; 	
										obj.z += velocity.z + windVector.z; 												
										
										
									};
																	
									function gainFocus(obj){

										if ( obj === undefined){
											oldSettings = null;
										}
										else{
											/*
											oldSettings = obj.component.material.emissive;
								            oldMaterial = obj.component.material.map;
								            if (oldMaterial != false){
								            	obj.component.material.map = THREE.ImageUtils.loadTexture( $scope.pathing_textures + 'checkerboard.jpg');
								            	obj.component.material.needsUpdate = true;
											}
											obj.component.material.emissive.setHex(hoverColor);
											*/
											if ( obj.outline != undefined){
												obj.outline.scale.set( scaleSize, scaleSize , scaleSize );
											}
										}
										
									};
									
									function lostFocus(obj){
										/*
										obj.component.material.map = oldMaterial;
										obj.component.material.emissive.setHex(oldSettings);
										*/
										if ( obj.outline != undefined){
											obj.outline.scale.set(.1,.1,.1);
										}
										oldSettings = null;
										
									};
														
									var i = $scope.canvasData.order.length; while (i--){
											
											var name = $scope.canvasData.order[i].name, 
												canvasData = $scope.canvasData[name],
												threeJS = canvasData.threeJS,
												usePostProcessing = canvasData.usePostProcessing,
												status = canvasData.status,
												render = canvasData.render,
												sceneComponents = canvasData.sceneComponents;
													

													var scene = $scope.canvasData[name].scene.scene,
														clock = $scope.canvasData[name].scene.clock,
														camera = threeJS.camera,
														renderer = threeJS.renderer,
														controls = threeJS.controls,
														stats = threeJS.stats,
                                                        allComponents = threeJS.allComponents,
														allParticles = threeJS.allParticles;



													
													
												
													// start
													var update = function(){
													if ( (render && $scope.isBrowserActive) || $scope.runInBackground){
														// begin stats
														stats.begin();
													
                                                            // do computations if not paused
                                                            if (status != 'pause'){
                                                                // particles
                                                                n = sceneComponents.particleCount; while(n--){

                                                                    root = allParticles["_" + n],
                                                                    particleSystem = root.particleSystem,
                                                                    particles = particleSystem.geometry.vertices,
                                                                    range = particleSystem.range,
                                                                    animation = particleSystem.animation,
                                                                        rotationSpd = animation.rotationSpd,
                                                                        velocityDefaults = animation.velocitySpd,
                                                                        maxVelocity = animation.maxVelocity,
                                                                    physics = particleSystem.physics;


                                                                    // movement
                                                                    particleSystem.rotation.x += rotationSpd.x;
                                                                    particleSystem.rotation.y += rotationSpd.y;
                                                                    particleSystem.rotation.z += rotationSpd.z;

                                                                    // individual particles
                                                                    var pCount = particles.length; while(pCount--){
                                                                            // get the particle
                                                                            var particle = particles[pCount];
                                                                            applyPhysics(particle, range, physics);

                                                                    }

                                                                }

                                                                n = sceneComponents.componentCount + 1; while(n--) {
                                                                    var componentCheck = allComponents["_" + n];
                                                                        componentCheck.component._events.onLoop.execute()
                                                                        if (componentCheck.outline != null){
                                                                            componentCheck.outline._events.onLoop.execute()
                                                                        }

                                                                }


                                                            }


															// check for raycaster
															var touching = $scope.canvas.intersectCheck();


															if (touching != false){

																// check to see if focused on anything
																if ($scope.touchingObject == null || ($scope.touchingObject != touching) ){
																	
																	// currently focused but swapped to new object
																	if ($scope.touchingObject != null){
																		lostFocus($scope.touchingObject);
																	}
																	// new focus
																	gainFocus(touching);
																	
																	
																}													
																$scope.touchingObject = touching;
																// do something code for touching	
															}
															else{
																// lose focus
																if ($scope.touchingObject != null){
																	lostFocus($scope.touchingObject);
																	
																}
																$scope.touchingObject = null;
															}
																									

														
															// controls

                                                                if (controls.enabled != undefined) {
                                                                    controls.enabled = false;
                                                                    if (name == $scope.activeCanvas || $scope.globalCamera) {
                                                                        controls.enabled = true;
                                                                    }
                                                                    if (controls.enabled) {
                                                                        if (!$scope.touchingWindow || $scope.touchingWindow == null || $scope.touchingWindow == undefined) {
                                                                            controls.update();
                                                                        }
                                                                    }
                                                                }


															// get new camera positions
															camera.defaults.rotation.x = (camera.rotation.x / Math.PI * 180);
															camera.defaults.rotation.y = (camera.rotation.y / Math.PI * 180);
															camera.defaults.rotation.z = (camera.rotation.z / Math.PI * 180);
															
															camera.defaults.position.x = camera.position.x;
															camera.defaults.position.y = camera.position.y;
															camera.defaults.position.z = camera.position.z;
	
															// update tweens										
															TWEEN.update( $scope.globalClock.getElapsedTime()*1000 );
																
															// use post processing or renderer
															renderer.render( scene, camera );

															if (canvasData.composer != undefined && usePostProcessing){															
																postProcessingHack(); // needs to be inluded
																canvasData.composer.render();		
															}
															
															stats.end();
														}
													};
													
													setInterval(function(){
														update();
													}, 1000 / scene.fps );		
													
											
									}
								};
								
								
								// INITIATE
								renderLoop();							
						},						
						
						////////////////////////////////
						togglePostProcessing:function(canvasName){
							
							var usePostProcessing = $scope.canvasData[canvasName].usePostProcessing;
							
							
							if (usePostProcessing == true){														
								$scope.canvas.disablePostProcessing(canvasName);								
							}else{			
								$scope.canvas.enablePostProcessing(canvasName);
							}
							
						},
										
						enablePostProcessing: function(canvasName){
							$scope.canvasData[canvasName].usePostProcessing = true;
						},

						disablePostProcessing: function(canvasName){	
							$scope.canvasData[canvasName].usePostProcessing = false;
						},
						////////////////////////////////
						
						
						////////////////////////////////
						togglePause:function(canvasName){
							var status = $scope.canvasData[canvasName].status;
							
							if (status != 'pause'){
								$scope.canvas.pauseAllScenes(canvasName);
							}else{
								$scope.canvas.unpauseAllScenes(canvasName);
							}
						},
												
						unpauseAllScenes: function(toggle){
							$scope.canvas.resumeClocks();
							var i = $scope.canvasData.order.length; while (i--){
									var name = $scope.canvasData.order[i].name; 	
									$scope.canvas.unpauseScene(name, toggle);
							}
						},

						pauseAllScenes: function(toggle){
							$scope.canvas.pauseClocks();
							var i = $scope.canvasData.order.length; while (i--){
									var name = $scope.canvasData.order[i].name; 	
									$scope.canvas.pauseScene(name, toggle);
							}
						},
						
						pauseScene:function(canvasName){
							$scope.canvasData[canvasName].status = 'pause';			
						},
						
						unpauseScene:function(canvasName){
							$scope.canvasData[canvasName].status = 'ready';							
						},					
						////////////////////////////////
						
						////////////////////////////////
						renderActiveOnly:function(){
							
							var i = $scope.canvasData.order.length; while (i--){
									var name = $scope.canvasData.order[i].name; 	
									
									if (name != $scope.activeCanvas){										
										$scope.canvas.stopRender(name);	
									}else{
										$scope.canvas.resumeRender(name);
									}								
							}	
						},						
						
						toggleRender:function(canvasName){
							
							var render = $scope.canvasData[canvasName].render;
							if (render){
								$scope.canvas.stopRender(canvasName);
							}else{
								$scope.canvas.resumeRender(canvasName);
							}
						},
						
						resumeRender:function(canvasName){
							$scope.canvasData[canvasName].render = true;													
						},
						
						stopRender:function(canvasName, toggle){
							$scope.canvasData[canvasName].render = false;
						},
						
						stopAllRender:function(){
							var i = $scope.canvasData.order.length; while (i--){
									var name = $scope.canvasData.order[i].name; 	
									$scope.canvas.stopRender(name);
							}
						},
						
						resumeAllRender:function(){
							var i = $scope.canvasData.order.length; while (i--){
									var name = $scope.canvasData.order[i].name; 	
									$scope.canvas.resumeRender(name);
							}
						},						
						////////////////////////////////
						
					};


				});

	    }
	    ///////////////////////////////////////
  };
});
